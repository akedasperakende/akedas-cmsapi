module.exports = {
	apps: [{
		name: 'akedasdb-cmsapi',
		script: 'yarn start',
		args: '',
		instances: 1,
		autorestart: true,
		watch: false,
		max_memory_restart: '8G',
		env: {
			NODE_ENV: 'development',
		},
		env_production: {
			NODE_ENV: 'production',
			HOST: 'https://testcmsapi.akedas.com.tr',
			BLOCK_IP: '',
			IPS: '',
			PORT: 8006,
			LOGLEVEL: 'error',
			NODEID: 'main',
			MAINNODE: 'main-1',
			DB: 'akedasdb',
            CDN:"https://testmobil.akedas.com.tr",
			MEDIAPATH:"/home/Mobuakedas/akedas-api/src",
			JWT_SECRET: 'akedaselektrikperakende',
			HASH_SECRET: 'akedaselektrikperakende',
			MONGO_URL: 'mongodb://localhost:27017/admin',
			ONESIGNAL_APPID:'',
			ONESIGNAL_APIKEY:'', 
			SMTP_HOST:"smtp.yandex.com",
			SMTP_PORT:"465",
			SMTP_EMAIL:"noreply@example.com",
			SMTP_PASSWORD:""
		}
	}
	]
};