'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const nanoid = require('nanoid');

let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/" + process.env.DB;
let dbo;

module.exports = {
	name: 'surveyAnswers',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL,
		{ useNewUrlParser: true, useUnifiedTopology: true }, process.env.DB),
	collection: 'surveyAnswers',


	settings: {
		entityValidator: {
			$$strict: true,
			_id: { type: 'string' },
			survey_id: { type: 'string' },
			user_id: 'string',
			created: 'date',
			completed: 'boolean',
			questions: {
				'type': 'array',
				items: {
					type: 'object',
					props: {
						type: { type: 'string', enum: ['S', 'F', 'M'] },
						name: 'string',
						is_answered: 'boolean',
						other: { type: 'string', optional: true },
						choices: {
							type: 'array',
							items: {
								type: 'object',
								props: {
									name: 'string',
									is_other: 'boolean',
									other: 'string',
									is_selected: 'boolean'
								}
							}
						}
					}
				}
			}
		}
	},

	actions: {
		async getResults() {

		},
		async chartData(ctx) {

			let survey_id = ctx.params.id;
			let questionName = ctx.params.questionName;

			let chartData = [];


			try {


				chartData = await this.adapter.db.collection("surveyAnswers").aggregate([
					{
						"$match": { "survey_id": survey_id }
					},
					{ "$unwind": "$questions" },
					{
						"$match": {
							"questions.name": questionName
						}
					},
					{ "$unwind": "$questions.choices" },
					{
						"$match": {
							"questions.choices.is_selected": true
						}
					},
					{
						"$group": {
							"_id": "$questions.choices.name",
							"questionName": { "$first": "$questions.name" },
							"survey_id": { "$first": "$survey_id" },
							"count": { "$sum": 1 }
						}
					},
					{ "$project": { "_id": 1, "questionName": 1, "survey_id": 1, "count": 1 } }

				]).toArray();



			} catch (err) {


			} finally {


			}


			return chartData;
		}

		// 	answerQuestion: {
		// 		handler: async (ctx) => {
		// 			let { survey_id, question_id, answers, other} = ctx.params;
		// 			let user_id = ctx.meta.user._id;

		// 			if (question_id && user_id && survey_id) {
		// 				question_id = Number(question_id);
		// 				let filledSurvey = await this.get(survey_id);

		// 				let question = filledSurvey.questions[question_id];

		// 				question.answers = question.answers || [];
		// 				question.is_answered = 1;

		// 				if(question.type === 'F'){
		// 					question.freetext = other;
		// 					question.is_selected = 1;
		// 				} 
		// 				else {
		// 					question.answers = answers.map(x => {
		// 						let q = question.choices[Number(x)];
		// 						q.is_selected = 1;
		// 						if(q.is_other == 1)
		// 							q.freetext = other;

		// 						return 1;
		// 					}).filter(x => x != null);
		// 					question.other = other;
		// 				}

		// 				let completed = true;
		// 				filledSurvey.questions.forEach(x => {
		// 					completed = completed && x.is_answered;
		// 				});

		// 				await this.adapter.updateById(survey_id, {$set: filledSurvey});

		// 				return true;
		// 			}else {
		// 				return new Error('parametreler eksik');
		// 			}
		// 		}
		// 	} 
	},

	hooks: {
		before: {
			create: [async (ctx) => {
				let x = ctx.params
				x.created = new Date();
				let questionList = await x.questions.map(q => {
					return { ...q, is_answered: true }

				})
				x.questions = questionList
				x._id = nanoid(25);
			}]
		},
		after: {
			create: async (ctx, result) => {
				let result_message = await ctx.call('resultMessage.get', { id: "0032" });
				result_message = {
					type: result_message.type,
					title: result_message.title,
					message: result_message.message
				};
				return { result, result_message };
			}
		}

	}
};