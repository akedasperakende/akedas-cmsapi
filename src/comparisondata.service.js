"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
let nanoid = require("nanoid");
var sql = require("mssql");
var config = {
  user: process.env.USER,
  password: process.env.PASSWORD,
  server: process.env.SERVER,
  database: process.env.DATABASE,
  dialect: process.env.DIALECT,
  connectionTimeout: 600000,
  requestTimeout: 600000,
  pool: {
    idleTimeoutMillis: 600000,
    max: 100,
  },
  dialectOptions: {
    instanceName: "SQLEXPRESS",
  },
  options: {
    trustedConnection: true,
    encrypt: true,
    enableArithAbort: true,
    trustServerCertificate: true,
  },
};
module.exports = {
  name: "comparisondata",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "musteri_kwh_kiyas",
  settings: {
    maxPageSize: 1000000,
  },
  actions: {
    async getmove() {
      let pool = await sql.connect(config);
      const request = pool.request();
      let comparisondatatable = await this.broker.call("comparisondata.count");
      let result = await request.query(
        "SELECT [_ILKODU] as il_kodu,[ILCE] as ilce,[MAHALLE] as mahalle,[Ocak Ortalama Tüketim (Kwh)] as Ocak_ort_tuketim,[Ocak Müşteri Tüketim (Kwh)] as Ocak_mus_tuketim,[Şubat Ortalama Tüketim (Kwh)] as Subat_ort_tuketim,[Şubat Müşteri Tüketim (Kwh)] as Subat_mus_tuketim,[Mart Ortalama Tüketim (Kwh)] as Mart_ort_tuketim,[Mart Müşteri Tüketim (Kwh)] as Mart_mus_tuketim,[Nisan Ortalama Tüketim (Kwh)] as Nisan_ort_tuketim,[Nisan Müşteri Tüketim (Kwh)] as Nisan_mus_tuketim,[Mayıs Ortalama Tüketim (Kwh)] as Mayis_ort_tuketim,[Mayıs Müşteri Tüketim (Kwh)] as Mayis_mus_tuketim,[Haziran Ortalama Tüketim (Kwh)] as Haziran_ort_tuketim,[Haziran Müşteri Tüketim (Kwh)] as Haziran_mus_tuketim,[Temmuz Ortalama Tüketim (Kwh)] as Temmuz_ort_tuketim,[Temmuz Müşteri Tüketim (Kwh)] as Temmuz_mus_tuketim,[Ağustos Ortalama Tüketim (Kwh)] as Agustos_ort_tuketim,[Ağustos Müşteri Tüketim (Kwh)] as Agustos_mus_tuketim,[Eylül Ortalama Tüketim (Kwh)] as Eylul_ort_tuketim,[Eylül Müşteri Tüketim (Kwh)] as Eylul_mus_tuketim,[Ekim Ortalama Tüketim (Kwh)] as Ekim_ort_tuketim,[Ekim Müşteri Tüketim (Kwh)] as Ekim_mus_tuketim,[Kasım Ortalama Tüketim (Kwh)] as Kasim_ort_tuketim,[Kasım Müşteri Tüketim (Kwh)] as Kasim_mus_tuketim,[Aralık Ortalama Tüketim (Kwh)] as Aralik_ort_tuketim,[Aralık Müşteri Tüketim (Kwh)] as Aralik_mus_tuketim,[SOZLESME_HESABI] as SOZLESME_HESAP_NO FROM musteri_kwh_kiyas_3"
      );
      if (comparisondatatable > 0 && result.recordset.length > 0) {
        await this.adapter.removeMany({});
        let comparisondata = await this.broker.call("comparisondata.count");
        if (comparisondata == 0) {
          let record = await result.recordset.map((r) => {
            (r._id = nanoid(25)),
            (r.il_kodu = String(r.il_kodu)),
              (r.ilce = String(r.ilce)),
              (r.mahalle = String(r.mahalle)),
              (r.Ocak_ort_tuketim = Number(r.Ocak_ort_tuketim)),
              (r.Ocak_mus_tuketim = Number(r.Ocak_mus_tuketim)),
              (r.Subat_ort_tuketim = Number(r.Subat_ort_tuketim)),
              (r.Subat_mus_tuketim = Number(r.Subat_mus_tuketim)),
              (r.Mart_ort_tuketim = Number(r.Mart_ort_tuketim)),
              (r.Mart_mus_tuketim = Number(r.Mart_mus_tuketim)),
              (r.Nisan_ort_tuketim = Number(r.Nisan_ort_tuketim)),
              (r.Nisan_mus_tuketim = Number(r.Nisan_mus_tuketim)),
              (r.Mayis_ort_tuketim = Number(r.Mayis_ort_tuketim)),
              (r.Mayis_mus_tuketim = Number(r.Mayis_mus_tuketim)),
              (r.Haziran_ort_tuketim = Number(r.Haziran_ort_tuketim)),
              (r.Haziran_mus_tuketim = Number(r.Haziran_mus_tuketim)),
              (r.Temmuz_ort_tuketim = Number(r.Temmuz_ort_tuketim)),
              (r.Temmuz_mus_tuketim = Number(r.Temmuz_mus_tuketim)),
              (r.Agustos_ort_tuketim = Number(r.Agustos_ort_tuketim)),
              (r.Agustos_mus_tuketim = Number(r.Agustos_mus_tuketim)),
              (r.Eylul_ort_tuketim = Number(r.Eylul_ort_tuketim)),
              (r.Eylul_mus_tuketim = Number(r.Eylul_mus_tuketim)),
              (r.Ekim_ort_tuketim = Number(r.Ekim_ort_tuketim)),
              (r.Ekim_mus_tuketim = Number(r.Ekim_mus_tuketim)),
              (r.Kasim_ort_tuketim = Number(r.Kasim_ort_tuketim)),
              (r.Kasim_mus_tuketim = Number(r.Kasim_mus_tuketim)),
              (r.Aralik_ort_tuketim = Number(r.Aralik_ort_tuketim)),
              (r.Aralik_mus_tuketim = Number(r.Aralik_mus_tuketim)),
              (r.SOZLESME_HESAP_NO = String(r.SOZLESME_HESAP_NO));
            return r;
          });
          await this.adapter.insertMany(record);
          return true;
        }
      }

      if (comparisondatatable == 0 && result.recordset.length > 0) {
        let record = await result.recordset.map((r) => {
          (r._id = nanoid(25)),
            (r.il_kodu = String(r.il_kodu)),
              (r.ilce = String(r.ilce)),
              (r.mahalle = String(r.mahalle)),
              (r.Ocak_ort_tuketim = Number(r.Ocak_ort_tuketim)),
              (r.Ocak_mus_tuketim = Number(r.Ocak_mus_tuketim)),
              (r.Subat_ort_tuketim = Number(r.Subat_ort_tuketim)),
              (r.Subat_mus_tuketim = Number(r.Subat_mus_tuketim)),
              (r.Mart_ort_tuketim = Number(r.Mart_ort_tuketim)),
              (r.Mart_mus_tuketim = Number(r.Mart_mus_tuketim)),
              (r.Nisan_ort_tuketim = Number(r.Nisan_ort_tuketim)),
              (r.Nisan_mus_tuketim = Number(r.Nisan_mus_tuketim)),
              (r.Mayis_ort_tuketim = Number(r.Mayis_ort_tuketim)),
              (r.Mayis_mus_tuketim = Number(r.Mayis_mus_tuketim)),
              (r.Haziran_ort_tuketim = Number(r.Haziran_ort_tuketim)),
              (r.Haziran_mus_tuketim = Number(r.Haziran_mus_tuketim)),
              (r.Temmuz_ort_tuketim = Number(r.Temmuz_ort_tuketim)),
              (r.Temmuz_mus_tuketim = Number(r.Temmuz_mus_tuketim)),
              (r.Agustos_ort_tuketim = Number(r.Agustos_ort_tuketim)),
              (r.Agustos_mus_tuketim = Number(r.Agustos_mus_tuketim)),
              (r.Eylul_ort_tuketim = Number(r.Eylul_ort_tuketim)),
              (r.Eylul_mus_tuketim = Number(r.Eylul_mus_tuketim)),
              (r.Ekim_ort_tuketim = Number(r.Ekim_ort_tuketim)),
              (r.Ekim_mus_tuketim = Number(r.Ekim_mus_tuketim)),
              (r.Kasim_ort_tuketim = Number(r.Kasim_ort_tuketim)),
              (r.Kasim_mus_tuketim = Number(r.Kasim_mus_tuketim)),
              (r.Aralik_ort_tuketim = Number(r.Aralik_ort_tuketim)),
              (r.Aralik_mus_tuketim = Number(r.Aralik_mus_tuketim)),
              (r.SOZLESME_HESAP_NO = String(r.SOZLESME_HESAP_NO));
          return r;
        });
        await this.adapter.insertMany(record);
        return true;
      }
    },
    async changeComparisonData(ctx) {

      let pool = await sql.connect(config);
      const request = pool.request();

      let today="musteri_kwh_kiyas_"+ctx.params.today
      let yesterday="musteri_kwh_kiyas_"+ctx.params.yesterday
   
      const result = await request.query(
        'SELECT [_ILKODU] as il_kodu,[ILCE] as ilce,[MAHALLE] as mahalle,'+
        '[Ocak Ortalama Tüketim (Kwh)] as Ocak_ort_tuketim,[Ocak Müşteri Tüketim (Kwh)] as Ocak_mus_tuketim,'+
        '[Şubat Ortalama Tüketim (Kwh)] as Subat_ort_tuketim,[Şubat Müşteri Tüketim (Kwh)] as Subat_mus_tuketim,'+
        '[Mart Ortalama Tüketim (Kwh)] as Mart_ort_tuketim,[Mart Müşteri Tüketim (Kwh)] as Mart_mus_tuketim,'+
        '[Nisan Ortalama Tüketim (Kwh)] as Nisan_ort_tuketim,[Nisan Müşteri Tüketim (Kwh)] as Nisan_mus_tuketim,'+
        '[Mayıs Ortalama Tüketim (Kwh)] as Mayis_ort_tuketim,[Mayıs Müşteri Tüketim (Kwh)] as Mayis_mus_tuketim,'+
        '[Haziran Ortalama Tüketim (Kwh)] as Haziran_ort_tuketim,[Haziran Müşteri Tüketim (Kwh)] as Haziran_mus_tuketim,'+
        '[Temmuz Ortalama Tüketim (Kwh)] as Temmuz_ort_tuketim,[Temmuz Müşteri Tüketim (Kwh)] as Temmuz_mus_tuketim,'+
        '[Ağustos Ortalama Tüketim (Kwh)] as Agustos_ort_tuketim,[Ağustos Müşteri Tüketim (Kwh)] as Agustos_mus_tuketim,'+
        '[Eylül Ortalama Tüketim (Kwh)] as Eylul_ort_tuketim,[Eylül Müşteri Tüketim (Kwh)] as Eylul_mus_tuketim,'+
        '[Ekim Ortalama Tüketim (Kwh)] as Ekim_ort_tuketim,[Ekim Müşteri Tüketim (Kwh)] as Ekim_mus_tuketim,'+
        '[Kasım Ortalama Tüketim (Kwh)] as Kasim_ort_tuketim,[Kasım Müşteri Tüketim (Kwh)] as Kasim_mus_tuketim,'+
        '[Aralık Ortalama Tüketim (Kwh)] as Aralik_ort_tuketim,[Aralık Müşteri Tüketim (Kwh)] as Aralik_mus_tuketim,'+
        '[SOZLESME_HESABI] as SOZLESME_HESAP_NO FROM'+today+
        'except'+
        'SELECT [_ILKODU] as il_kodu,[ILCE] as ilce,[MAHALLE] as mahalle,'+
        '[Ocak Ortalama Tüketim (Kwh)] as Ocak_ort_tuketim,[Ocak Müşteri Tüketim (Kwh)] as Ocak_mus_tuketim,'+
        '[Şubat Ortalama Tüketim (Kwh)] as Subat_ort_tuketim,[Şubat Müşteri Tüketim (Kwh)] as Subat_mus_tuketim,'+
        '[Mart Ortalama Tüketim (Kwh)] as Mart_ort_tuketim,[Mart Müşteri Tüketim (Kwh)] as Mart_mus_tuketim,'+
        '[Nisan Ortalama Tüketim (Kwh)] as Nisan_ort_tuketim,[Nisan Müşteri Tüketim (Kwh)] as Nisan_mus_tuketim,'+
        '[Mayıs Ortalama Tüketim (Kwh)] as Mayis_ort_tuketim,[Mayıs Müşteri Tüketim (Kwh)] as Mayis_mus_tuketim,'+
        '[Haziran Ortalama Tüketim (Kwh)] as Haziran_ort_tuketim,[Haziran Müşteri Tüketim (Kwh)] as Haziran_mus_tuketim,'+
        '[Temmuz Ortalama Tüketim (Kwh)] as Temmuz_ort_tuketim,[Temmuz Müşteri Tüketim (Kwh)] as Temmuz_mus_tuketim,'+
        '[Ağustos Ortalama Tüketim (Kwh)] as Agustos_ort_tuketim,[Ağustos Müşteri Tüketim (Kwh)] as Agustos_mus_tuketim,'+
        '[Eylül Ortalama Tüketim (Kwh)] as Eylul_ort_tuketim,[Eylül Müşteri Tüketim (Kwh)] as Eylul_mus_tuketim,'+
        '[Ekim Ortalama Tüketim (Kwh)] as Ekim_ort_tuketim,[Ekim Müşteri Tüketim (Kwh)] as Ekim_mus_tuketim,'+
        '[Kasım Ortalama Tüketim (Kwh)] as Kasim_ort_tuketim,[Kasım Müşteri Tüketim (Kwh)] as Kasim_mus_tuketim,'+
        '[Aralık Ortalama Tüketim (Kwh)] as Aralik_ort_tuketim,[Aralık Müşteri Tüketim (Kwh)] as Aralik_mus_tuketim,'+
        '[SOZLESME_HESABI] as SOZLESME_HESAP_NO FROM '+yesterday
      );

      let  record = await result.recordset.map((r) => {
        (r._id = nanoid(25)),
        (r.il_kodu = String(r.il_kodu)),
          (r.ilce = String(r.ilce)),
          (r.mahalle = String(r.mahalle)),
          (r.Ocak_ort_tuketim = Number(r.Ocak_ort_tuketim)),
          (r.Ocak_mus_tuketim = Number(r.Ocak_mus_tuketim)),
          (r.Subat_ort_tuketim = Number(r.Subat_ort_tuketim)),
          (r.Subat_mus_tuketim = Number(r.Subat_mus_tuketim)),
          (r.Mart_ort_tuketim = Number(r.Mart_ort_tuketim)),
          (r.Mart_mus_tuketim = Number(r.Mart_mus_tuketim)),
          (r.Nisan_ort_tuketim = Number(r.Nisan_ort_tuketim)),
          (r.Nisan_mus_tuketim = Number(r.Nisan_mus_tuketim)),
          (r.Mayis_ort_tuketim = Number(r.Mayis_ort_tuketim)),
          (r.Mayis_mus_tuketim = Number(r.Mayis_mus_tuketim)),
          (r.Haziran_ort_tuketim = Number(r.Haziran_ort_tuketim)),
          (r.Haziran_mus_tuketim = Number(r.Haziran_mus_tuketim)),
          (r.Temmuz_ort_tuketim = Number(r.Temmuz_ort_tuketim)),
          (r.Temmuz_mus_tuketim = Number(r.Temmuz_mus_tuketim)),
          (r.Agustos_ort_tuketim = Number(r.Agustos_ort_tuketim)),
          (r.Agustos_mus_tuketim = Number(r.Agustos_mus_tuketim)),
          (r.Eylul_ort_tuketim = Number(r.Eylul_ort_tuketim)),
          (r.Eylul_mus_tuketim = Number(r.Eylul_mus_tuketim)),
          (r.Ekim_ort_tuketim = Number(r.Ekim_ort_tuketim)),
          (r.Ekim_mus_tuketim = Number(r.Ekim_mus_tuketim)),
          (r.Kasim_ort_tuketim = Number(r.Kasim_ort_tuketim)),
          (r.Kasim_mus_tuketim = Number(r.Kasim_mus_tuketim)),
          (r.Aralik_ort_tuketim = Number(r.Aralik_ort_tuketim)),
          (r.Aralik_mus_tuketim = Number(r.Aralik_mus_tuketim)),
          (r.SOZLESME_HESAP_NO = String(r.SOZLESME_HESAP_NO));
      return r;
      });
      
      let dataInsert=await this.adapter.insertMany(record);
      return dataInsert.length
    },
    async deleteComparisonData(ctx) {


      let pool = await sql.connect(config);
      const request = pool.request();

      let today="musteri_kwh_kiyas_"+ctx.params.today
      let yesterday="musteri_kwh_kiyas_"+ctx.params.yesterday

      const partial_result = await request.query(
        'SELECT [_ILKODU] as il_kodu,[ILCE] as ilce,[MAHALLE] as mahalle,'+
        '[Ocak Ortalama Tüketim (Kwh)] as Ocak_ort_tuketim,[Ocak Müşteri Tüketim (Kwh)] as Ocak_mus_tuketim,'+
        '[Şubat Ortalama Tüketim (Kwh)] as Subat_ort_tuketim,[Şubat Müşteri Tüketim (Kwh)] as Subat_mus_tuketim,'+
        '[Mart Ortalama Tüketim (Kwh)] as Mart_ort_tuketim,[Mart Müşteri Tüketim (Kwh)] as Mart_mus_tuketim,'+
        '[Nisan Ortalama Tüketim (Kwh)] as Nisan_ort_tuketim,[Nisan Müşteri Tüketim (Kwh)] as Nisan_mus_tuketim,'+
        '[Mayıs Ortalama Tüketim (Kwh)] as Mayis_ort_tuketim,[Mayıs Müşteri Tüketim (Kwh)] as Mayis_mus_tuketim,'+
        '[Haziran Ortalama Tüketim (Kwh)] as Haziran_ort_tuketim,[Haziran Müşteri Tüketim (Kwh)] as Haziran_mus_tuketim,'+
        '[Temmuz Ortalama Tüketim (Kwh)] as Temmuz_ort_tuketim,[Temmuz Müşteri Tüketim (Kwh)] as Temmuz_mus_tuketim,'+
        '[Ağustos Ortalama Tüketim (Kwh)] as Agustos_ort_tuketim,[Ağustos Müşteri Tüketim (Kwh)] as Agustos_mus_tuketim,'+
        '[Eylül Ortalama Tüketim (Kwh)] as Eylul_ort_tuketim,[Eylül Müşteri Tüketim (Kwh)] as Eylul_mus_tuketim,'+
        '[Ekim Ortalama Tüketim (Kwh)] as Ekim_ort_tuketim,[Ekim Müşteri Tüketim (Kwh)] as Ekim_mus_tuketim,'+
        '[Kasım Ortalama Tüketim (Kwh)] as Kasim_ort_tuketim,[Kasım Müşteri Tüketim (Kwh)] as Kasim_mus_tuketim,'+
        '[Aralık Ortalama Tüketim (Kwh)] as Aralik_ort_tuketim,[Aralık Müşteri Tüketim (Kwh)] as Aralik_mus_tuketim,'+
        '[SOZLESME_HESABI] as SOZLESME_HESAP_NO FROM'+yesterday+
        'except'+
        'SELECT [_ILKODU] as il_kodu,[ILCE] as ilce,[MAHALLE] as mahalle,'+
        '[Ocak Ortalama Tüketim (Kwh)] as Ocak_ort_tuketim,[Ocak Müşteri Tüketim (Kwh)] as Ocak_mus_tuketim,'+
        '[Şubat Ortalama Tüketim (Kwh)] as Subat_ort_tuketim,[Şubat Müşteri Tüketim (Kwh)] as Subat_mus_tuketim,'+
        '[Mart Ortalama Tüketim (Kwh)] as Mart_ort_tuketim,[Mart Müşteri Tüketim (Kwh)] as Mart_mus_tuketim,'+
        '[Nisan Ortalama Tüketim (Kwh)] as Nisan_ort_tuketim,[Nisan Müşteri Tüketim (Kwh)] as Nisan_mus_tuketim,'+
        '[Mayıs Ortalama Tüketim (Kwh)] as Mayis_ort_tuketim,[Mayıs Müşteri Tüketim (Kwh)] as Mayis_mus_tuketim,'+
        '[Haziran Ortalama Tüketim (Kwh)] as Haziran_ort_tuketim,[Haziran Müşteri Tüketim (Kwh)] as Haziran_mus_tuketim,'+
        '[Temmuz Ortalama Tüketim (Kwh)] as Temmuz_ort_tuketim,[Temmuz Müşteri Tüketim (Kwh)] as Temmuz_mus_tuketim,'+
        '[Ağustos Ortalama Tüketim (Kwh)] as Agustos_ort_tuketim,[Ağustos Müşteri Tüketim (Kwh)] as Agustos_mus_tuketim,'+
        '[Eylül Ortalama Tüketim (Kwh)] as Eylul_ort_tuketim,[Eylül Müşteri Tüketim (Kwh)] as Eylul_mus_tuketim,'+
        '[Ekim Ortalama Tüketim (Kwh)] as Ekim_ort_tuketim,[Ekim Müşteri Tüketim (Kwh)] as Ekim_mus_tuketim,'+
        '[Kasım Ortalama Tüketim (Kwh)] as Kasim_ort_tuketim,[Kasım Müşteri Tüketim (Kwh)] as Kasim_mus_tuketim,'+
        '[Aralık Ortalama Tüketim (Kwh)] as Aralik_ort_tuketim,[Aralık Müşteri Tüketim (Kwh)] as Aralik_mus_tuketim,'+
        '[SOZLESME_HESABI] as SOZLESME_HESAP_NO FROM '+today
      );

      let rem = await this.adapter.removeMany({
            SOZLESME_HESAP_NO: {
               $in: partial_result.recordset.map((x) => x.SOZLESME_HESAP_NO)
          }
          });
      return rem

    },
  },
};
