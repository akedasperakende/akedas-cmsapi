'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const nanoid = require("nanoid");
const dayjs = require("dayjs");

module.exports = {
    name: 'paymentLog',
    mixins: [DbService],
    adapter: new MongoDBAdapter(process.env.MONGO_URL,
        {useNewUrlParser: true, useUnifiedTopology: true}, process.env.DB),
    collection: 'paymentLog',

    settings: {
        entityValidator: {
            $$strict: true,
            _id: {type: 'string'},
            date: 'date',
            createdAt: 'date',
            updatedAt: 'date',
            price: {type: "number"},

        }
    },
    hooks: {
        before: {
            create: [(ctx => {
                ctx.params._id = nanoid(25);
                ctx.params.price=ctx.params.price
                ctx.params.createdAt = new Date();
                ctx.params.updatedAt = new Date();
                ctx.params.date =new Date();
            })],
        }
    }
};