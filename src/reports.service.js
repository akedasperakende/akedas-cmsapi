'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
let xlxs = require('xlsx');
let dayjs = require('dayjs');


let surveys = {
   _id: 'surveys',
   title: 'Anketler',
   sort: '-startDate',
   report: 'survey',
   fields: [
      {name: 'name', title: 'Başlık', list: 1, type: 'text', required: true},
      {name: 'active', title: 'Aktif', list: 1, type: 'boolean', required: true},
      {name: 'startDate', end: 'endDate', list: 1, title: 'Başlangıç/Bitiş Tarihi', type: 'date', required: true}
   ]
};

let notifications = {
   _id: 'pushNotifications',
   title: 'Bildirimler',
   sort: '-date',
   fields: [
      {name: 'title', title: 'Başlık', list: 1, type: 'text', required: true},
      {name: 'active', title: 'Aktif', list: 1, type: 'boolean', required: true},
      {name: 'date', title: 'Gönderim Zamanı', List: 1, type: 'date', required: true},
      {name: 'content', title: 'İçerik', type: 'text', required: true},
      {name: 'type', title: 'Açılacak Sayfa', type: 'module'},
      {name: 'icon', title: 'İkon', type: 'file', multi: false, ext: ['image'], required: true},
      {name: 'media', title: 'Görsel', type: 'file', multi: false, required: true}
   ]
};


let banners = {
   _id: 'banners',
   title: 'Bannerler',
   sort: '-startDate',
   filters: [
      {name: 'archive', title: 'Arşiv', type: 'boolean'}
   ],
   fields: [
      {name: 'title', title: 'İsim', list: 1, type: 'text'},
      {name: 'order', title: 'Sıra', list: 1, type: 'number', required: true},
      {name: 'active', title: 'Aktif', list: 1, type: 'boolean', required: true},
      {name: 'startDate', end: 'endDate', list: 1, title: 'Başlangıç/Bitiş Tarihi', type: 'date', required: true},
      {name: 'registeredUsers', title: 'Kullanıcılar (Sicil No)', type: 'registeredUsers', required: false},
      {name: 'type', title: 'Açılacak Sayfa', type: 'module'},
      {name: 'media', title: 'Banner', type: 'file', multi: false, required: true}
   ]
};


let registeredUsers = {
   _id: 'registeredUsers',
   title: 'Kullanıcılar',
   sort: 'name',
   listOnly: true,
   fields: [
      {name: '_id', title: 'Sicil', list: 1, type: 'text'},
      {name: 'name', title: 'İsim', list: 1, type: 'text'}
   ]
};

let resources = {
   surveys,
   notifications,
   banners,
   registeredUsers
};

module.exports = {
   name: 'reports',
   mixins: [DbService],
   adapter: new MongoDBAdapter(process.env.MONGO_URL,
      {useNewUrlParser: true, useUnifiedTopology: true}, process.env.DB),
   collection: 'reports',

   actions: {
      async report(ctx) {
         let resource = resources[ctx.params.resource];
         let data = await this.broker.call(ctx.params.resource + '.find',
            {
               fields: resource.fields.map(x => x.name)
            });

         data.forEach(d => {
            let result = {};
            resources.fields.forEach(({name, type, end, title}) => {
               if (type == 'date') {
                  if (!end) result[title] = dayjs(d[name]);
                  if (end) result[title] = dayjs(d[name]) + ' - ' + dayjs(d[end]);
               } else if (type == 'text') result[title] = d[name];
               else if (type == 'number') result[title] = d[name];
            });
         });

         let ws = xlxs.utils.json_to_sheet(data, {
            headers: data.length > 0 ? Object.getOwnPropertyNames(data[0]) : []
         });

         ctx.meta.$responseType = 'text/xls';
         return xlxs.stream.to_csv(ws, {
            FS: ';'
         });
      },

      async survey(ctx) {

         let survey = await this.broker.call('surveys.get', {id: ctx.params.id});
         let answers = await this.broker.call('surveyAnswers.find', {
            query: {survey_id: survey._id}
         });
         let registeredUsers = await this.broker.call('registeredUsers.find', {
            query: {_id: {$in: answers.map(a => a.registered_user_id)}}
         });

         let rows = [];
         for (const answer of answers) {
            let user = registeredUsers.find(a => a._id === answer.registered_user_id);
            let [subscriber] = await ctx.broker.call('subscriber.getByField', {
               field: user.isCorporate === true ? "VERGI_NO" : "TC_NUMARASI",
               value: user.isCorporate === true ? user.vkNo : (user.tcNo || user.tcno)
            }, {meta: ctx.meta});
            let row = {
               'Anket': survey.name,
               'Ad Soyad': subscriber ? [subscriber.AD || "", subscriber.SOYAD || ""].filter(x => x).join(' ') : "Kullanıcı Bulunamadı",
               'TC': user && !!user.tcNo ? user.tcNo : " - ",
               'Vergi No': subscriber && !!subscriber.VERGI_NO ? subscriber.VERGI_NO : " - ",
               'Cep Telefonu': subscriber && !!subscriber.TEL_NO ? "tel: " + subscriber.TEL_NO : ' - ',
               'Email': subscriber && !!subscriber.MAIL ? subscriber.MAIL : ' - ',
               'Zaman': dayjs(answer.created).format('DD-MM-YYYY HH:mm:ss:SSS'),
            };

            answer.questions.forEach(q => {
               row[q.name] = '';
               if (q.type === 'F') {
                  row[q.name] = q.other;
               } else if (q.choices) {
                  let selects = q.choices.filter(c => c.is_selected);
                  row[q.name] = selects.map(x => x.name + (x.is_other ? `'${x.other}'` : '')).join(', ');
               }
            });
            rows.push(row);
         }

         let ws = xlxs.utils.json_to_sheet(rows, {
            header: rows.length > 0 ? Object.getOwnPropertyNames(rows[0]) : []
         });

         ctx.meta.$responseType = 'text/xls';
         return xlxs.stream.to_csv(ws, {
            FS: ';'
         });
      },
      async pushNotifications(ctx) {
         let pushNotification = await this.broker.call('pushNotifications.get', {id: ctx.params.id});
         let notification = await this.broker.call('notifications.find', {
            query: {
               pushNotificationId: pushNotification._id
            }
         });
         let rows = [];
         for (const x of notification) {
            let user = await this.broker.call("registeredUsers.find", {
               query: {
                  _id: x.registeredUserId
               }
            })
            let _subscriber;
            if (user && user.length > 0) {
               user = user[0]
               let [subscriber] = await ctx.broker.call('subscriber.getByField', {
                  field: user.isCorporate === true ? "VERGI_NO" : "TC_NUMARASI",
                  value: user.isCorporate === true ? user.vkNo : (user.tcNo || user.tcno)
               }, {meta: ctx.meta});
               _subscriber = subscriber
            }
            let row = {
               'Başlık': pushNotification ? pushNotification.content : " - ",
               'Ad Soyad': _subscriber ? [_subscriber.AD || "", _subscriber.SOYAD || ""].filter(x => x).join(' ') : "Kullanıcı Bulunamadı",
               'Cep Telefonu': _subscriber && !!_subscriber.TEL_NO ? "tel: " + _subscriber.TEL_NO : ' - ',
               'TC': user && !!user.tcNo ? user.tcNo : ' - ',
               'Vergi No': _subscriber && !!_subscriber.VERGI_NO ? _subscriber.VERGI_NO : ' - ',
               'Email': _subscriber && !!_subscriber.MAIL ? _subscriber.MAIL : ' - ',
               'Durumu': x.isRead ? 'Okundu' : 'Okunmadı',
            };

            rows.push(row);
         }

         let ws = xlxs.utils.json_to_sheet(rows, {
            header: rows.length > 0 ? Object.getOwnPropertyNames(rows[0]) : []
         });

         ctx.meta.$responseType = 'text/xls';
         return xlxs.stream.to_csv(ws, {
            FS: ';'
         });
      },
      async requestAndComplaintCategories(ctx) {
         let category = await ctx.call('requestAndComplaintCategories.get', {id: ctx.params.id})

         let userMessage = await ctx.call('requestAndComplaint.find', {
            query: {
               categoryId: category._id
            },
            sort: '-createdAt'
         })
         userMessage = userMessage.map(x => {
            return {
               "Ad Soyad": (x.registeredUserId.name || "") + " " + (x.registeredUserId.lastname || ""),
               "TC": !!x.registeredUserId.tcNo ? x.registeredUserId.tcNo : " - ",
               "Vergi No": !!x.registeredUserId.VERGI_NO ? x.registeredUserId.VERGI_NO : " - ",
               "Sözleşme Hesap No": !!x.SOZLESME_HESAP_NO ? x.SOZLESME_HESAP_NO : " - ",
               "Alt Kategori": !!x.subCategoryId.name ? x.subCategoryId.name : " - ",
               "Email": !!x.registeredUserId.email ? x.registeredUserId.email : " - ",
               "Telefon": !!x.registeredUserId.phone ? "tel: " + x.registeredUserId.phone : ' - ',
               "Adres": !!x.address ? x.address : " - ",
               "Mesaj": !!x.message ? x.message : " - ",
               "Oluşturma Tarih": userMessage ? dayjs(x.createdAt).format('DD/MM/YYYY HH:mm:ss:SSS') : '-',
            }
         })
         let ws = xlxs.utils.json_to_sheet(userMessage);

         ctx.meta.$responseType = 'text/xls';
         return xlxs.stream.to_csv(ws, {
            FS: ';'
         });

      },
      async requestAndComplaintSubCategories(ctx) {
         let category = await ctx.call('requestAndComplaintSubCategories.get', {id: ctx.params.id})

         let userMessage = await ctx.call('requestAndComplaint.find', {
            query: {
               subCategoryId: category._id
            },
            sort: '-createdAt'
         })
         userMessage = userMessage.map(x => {
            return {
               "Ad Soyad": (x.registeredUserId.name || "") + " " + (x.registeredUserId.lastname || ""),
               "TC": !!x.registeredUserId.tcNo ? x.registeredUserId.tcNo : " - ",
               "Vergi No": !!x.registeredUserId.VERGI_NO ? x.registeredUserId.VERGI_NO : " - ",
               "Sözleşme Hesap No": !!x.SOZLESME_HESAP_NO ? x.SOZLESME_HESAP_NO : " - ",
               "Email": !!x.registeredUserId.email ? x.registeredUserId.email : " - ",
               "Telefon": !!x.registeredUserId.phone ? "tel: " + x.registeredUserId.phone : ' - ',
               "Adres": !!x.address ? x.address : " - ",
               "Mesaj": !!x.message ? x.message : " - ",
               "Oluşturma Tarih": userMessage ? dayjs(x.createdAt).format('DD/MM/YYYY HH:mm:ss:SSS') : '-',
            }
         })
         let ws = xlxs.utils.json_to_sheet(userMessage);
         ctx.meta.$responseType = 'text/xls';
         return xlxs.stream.to_csv(ws, {
            FS: ';'
         });

      },
      async logins(ctx) {
         let logins = await ctx.call('login.find', {
            fields: ["userId", "name", "email", "lastname", "date", "role", 'type'],
            sort: "userId,date"
         })

         logins = logins.map(l => ({
            "Person Id": l.userId,
            "Ad Soyad": l.name + " " + l.lastname,
            "Eposta": l.email,
            "Zaman": dayjs(l.date).add(4, 'hour').format('DD-MM-YYYY HH:mm:ss:ms'),
            "Rol": l.role,
            'Tip': l.type || 'Giriş'
         }))

         let ws = xlxs.utils.json_to_sheet(logins);

         ctx.meta.$responseType = 'text/xls';
         return xlxs.stream.to_csv(ws, {
            FS: ';'
         });

      },
      async loggedInUsers(ctx) {
         let registeredUsers = await ctx.call('registeredUsers.find', {
            query: {
               deleted: {$ne: true}
            },
            fields: ["_id", "name", "email", "lastname", "role"]
         })
         let logins = await ctx.call('login.find', {
            fields: ["userId", "name", "email", "lastname", "date", "role", 'type'],
            sort: "userId,date"
         })

         let result = registeredUsers.map((user) => {
            let log = logins.find(l => l.userId === user._id ? l : null);
            return ({...user, log})
         })


         logins = result.map(r => ({
            "Person Id": r._id,
            'Ad Soyad': r ? [r.name, r.lastname].filter(d => d).join(' ') : "Kullanıcı Bulunamadı",
            "Eposta": r.email,
            "Rol": r.role,
            "Giriş Bilgisi": r.log ? "GİRİŞ YAPILDI" : "GİRİŞ YAPILMADI",
            "Zaman": r.log ? dayjs(r.log.date).format('DD-MM-YYYY HH:mm:ss:ms') : "Giriş Bilgisi Bulunamadı.",
            'Tip': r.log ? r.log.type || "GİRİŞ" : 'Giriş Bilgisi Bulunamadı.'

         }))

         let ws = xlxs.utils.json_to_sheet(logins);

         ctx.meta.$responseType = 'text/xls';
         return xlxs.stream.to_csv(ws, {
            FS: ';'
         });
      },
      async coordinates(ctx) {
         let items = await ctx.call('coordinates.find', {})

         items = items.map(i => ({
            "İl": i.city,
            "Kurum Adı": i.name,
            "Adresi": i.address,
            "İlçe": i.district,
            "Semt": i.locality,
            "Telefonu": "tel: " + i.phone,
            "Kurum Kodu": i._id,
         }))

         let ws = xlxs.utils.json_to_sheet(items);

         ctx.meta.$responseType = 'text/xls';
         return xlxs.stream.to_csv(ws, {
            FS: ';'
         });

      }
   }
};
