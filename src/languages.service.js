'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const nanoid = require('nanoid');

module.exports = {
	name: 'languages',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'languages',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			name: 'string',
			items: 'object'
		}
	},

	hooks: {
		before: {
			create: [ctx => {
				ctx.params._id = nanoid(25);
			}]
		}
	}
};