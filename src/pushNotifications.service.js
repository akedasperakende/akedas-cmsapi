'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
let dayjs = require('dayjs');
let nanoid = require('nanoid');

let icon = {
	'_id': 'notification.png',
	'url': process.env.CDN + '/general/notification.png',
	'thumb': process.env.CDN + '/general/notification.png',
	'type': 'image',
	'mimeType': 'image/png'
}

module.exports = {
	name: 'pushNotifications',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL,
		{ useNewUrlParser: true, useUnifiedTopology: true }, process.env.DB),
	collection: 'pushNotifications',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: { type: 'string' },
			lang: { type: 'string', optional: true },
			title: { type: 'string' },
			active: 'boolean',
			isIndividual: 'boolean',
			muhatapNo:{ type: 'string', optional: true },
		//	pin: { type: 'boolean', optional: true },
			sendNow: { type: 'boolean', optional: true },
			sendNotification: { type: 'boolean', optional: true },
			completed: 'boolean',
			content: 'string',
			lang: { type: 'string', optional: true },
			notifyType: { type: 'string', optional: true },
			item_id: { type: 'string', optional: true },
			date: 'date',
			created: 'date',
			createdBy: 'string',
			createdAt: 'date',
			updatedAt: 'date',
			updatedBy: 'string',
			registeredUsers: { type: 'array', optional: true, items: 'string' },
			area: [{ type: 'array', items: 'string', optional: true }],
			district: [{ type: 'array', items: 'string', optional: true }],
			neighbourhood: [{ type: 'array', items: 'string', optional: true }],
			//groups: [{ type: 'array', items: 'string', optional: true }],
			// usersFile: {
			// 	'type': 'object', optional: true, props: {
			// 		type: { type: 'string' },
			// 		thumb: { type: 'string' },
			// 		url: { type: 'string' },
			// 		_id: { type: 'string' },
			// 		mimeType: { type: 'string' }
			// 	}
			// },
			// type: {
			// 	type: 'string', enum: ['not_modul', 'notifications', "pushNotifications", 'survey','banners', 'user']
			// },
			icon: {
				'type': 'object', props: {
					type: { type: 'string' },
					thumb: { type: 'string' },
					url: { type: 'string' },
					_id: { type: 'string' },
					mimeType: { type: 'string' }
				}
			},
			// media: {
			// 	'type': 'object', props: {
			// 		type: { type: 'string' },
			// 		thumb: { type: 'string' },
			// 		url: { type: 'string' },
			// 		_id: { type: 'string' },
			// 		mimeType: { type: 'string' }
			// 	}
			// }
		}
	},

	actions: {
		async archive() {
			this.adapter.updateMany({
				date: { $lte: new Date() }
			}, {
				$set: {
					archive: true
				}
			});
		},
		async itemsRemove(ctx) {
			let item_id = ctx.params.item_id;

			let many = await this.adapter.removeMany({
				item_id: item_id
			});

		},
	},

	hooks: {
		before: {
			list: async ctx => {
				//sadece cmsten
				let search = ctx.params.search
				if (search && search.length > 2) {
					let result = await ctx.broker.call('search.pushNotificationSearch', {text: search})
					if (!ctx.params.query)
						ctx.params.query = {}
					ctx.params.query._id = {$in: result.map(r => r._id)}
				}
				delete ctx.params.search;
				ctx.params.populate = ['groups'];
				if (!ctx.params.query) {
					ctx.params.query = {}
				}
				ctx.params.query.notifyType = 'manuel';
				let query = ctx.params.query;

				if (query && query.archive === 'true') {
					query.archive = query && query.archive === 'true';
				}
				else if (query) {
					query.archive = null;
				}

			},
			create: [ctx => {
				ctx.params._id = nanoid(25);
				ctx.params.date = dayjs(ctx.params.date || new Date()).toDate();
				ctx.params.created = new Date();
				ctx.params.createdAt = dayjs().toDate();
				ctx.params.createdBy = ctx.meta.user._id;
				ctx.params.updatedAt = dayjs().toDate();
				ctx.params.updatedBy = ctx.meta.user._id;
				ctx.params.registeredUsers = ctx.params.registeredUsers || [];
				// ctx.params.type = ctx.params.type || 'notifications';
				// if (ctx.params.type == "not_modul" || ctx.params.type == "notifications" ) {
				// 	ctx.params.type = 'pushNotifications';
				// }
				ctx.params.notifyType = 'manuel';
				ctx.params.completed = false;

				let icon = {
					'_id': ctx.params._id,
					'url': process.env.CDN + '/files/pushNotifications.png',
					'thumb': process.env.CDN + '/files/pushNotifications.png',
					'type': 'image',
					'mimeType': 'image/png'
				}

				ctx.params.icon = ctx.params.icon || icon;
				//ctx.params.media = ctx.params.media || icon;

				// if (ctx.params.type == "pushNotifications") {
				// 	ctx.params.type = 'notifications';
				// }
				// if (ctx.params.active == false && ctx.params.sendNotification == true) {
				// 	ctx.params.type = 'postwall';
				// }
				// if (ctx.params.pin === true) {
				// 	ctx.params.sendNotification = false
				// 	ctx.params.completed = true;
				// 	ctx.params.sendNow = false
				// }
				if (ctx.params.sendNow === true) {
					ctx.params.sendNotification = false
					ctx.params.completed = true;
				}
			}],

			update: [ctx => {
				let date = dayjs(ctx.params.date);
				ctx.params.date = date.toDate();
				ctx.params.registeredUsers = ctx.params.registeredUsers || [];
				// ctx.params.type = ctx.params.type || 'notifications';
				// if (ctx.params.type == "pushNotifications") {
				// 	ctx.params.type = 'notifications';
				// }
				ctx.params.notifyType = 'manuel';
				// if(date.isAfter(dayjs(new Date())))
				// 	ctx.params.completed = false;
				ctx.params.updatedAt = dayjs().toDate();
				if(ctx.meta.user){
					ctx.params.updatedBy = ctx.meta.user._id;
				}
				delete ctx.params.createdAt;
				delete ctx.params.createdBy;
			}],
			remove: [async ctx => {
				let romeves = await ctx.broker.call('notifications.pushNotificationRemove', { pushNotificationId: ctx.params.id });
			}]
		},

		after: {
			update: (ctx, result) => {
				ctx.call('notifications.updateAll', {
					pushNotificationId: result._id,
					title: result.title,
					active: result.active,
					sendNotification: result.sendNotification,
					content: result.content,
					icon: result.icon,
				//	media: result.media,
					// type: result.type
				})
				return result;
			},
			create: async (ctx, result) => {
				
				if (ctx.params.pin === true) {
					ctx.call('notifications.create', {
						isRead: false,
						userId: "",
						title: ctx.params.title,
						active: ctx.params.active,
						sendNotification: false,
						pushNotificationId: ctx.params._id,
						item_id: '',
						content: ctx.params.content,
						date: ctx.params.date,
						icon: ctx.params.icon,
					//	media: ctx.params.media,
					//	type: ctx.params.type,
					//	groups: ctx.params.groups,
						lang: ctx.params.lang,
						subtype: '',
						readUsers: []
					},
						{ meta: { user: ctx.meta.user } });
				}

				if(ctx.params.sendNow === true){
					ctx.params.sendNotification = true
					await ctx.broker.call('pushNotificationSendNow.SendNow',{...ctx.params},{ meta: { user: ctx.meta.user } });
					ctx.params.sendNotification == false
				}
				
				return result;

			}
		}
	}
};