'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
let axios = require('axios');

module.exports = {
	name: 'pushNotificationSendNow',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL,
		{ useNewUrlParser: true, useUnifiedTopology: true }, process.env.DB),
	collection: 'pushNotifications',
    
	actions: {
		async SendNow(ctx) {
            let notifications = [ctx.params]

            notifications.forEach(async n => {
                let registeredUsers = [];
                this.broker.call('pushNotifications.update', {
                    ...n,
                    completed: true
                });
                // //sadece ikiside  etkiketliyse
                // if (!(n.registeredUsers == null || n.registeredUsers.length == 0) && !(n.groups == null || n.groups.length == 0)) {
                //     let groupusers = [];
                //     registeredUsers = await ctx.broker.call('registeredUsers.find', {
                //         query: { _id: { $in: n.registeredUsers } },
                //         fields: ['_id', 'playerId', 'lang']
                //     });
                //     let queryQuery = n.groups.map(function (elem) {
                //         return { "groups": { "$all": [elem] } };
                //     })
                //     groupusers = await ctx.broker.call('registeredUsers.find', {
                //         query: { "$or": queryQuery },
                //         fields: ['_id', 'playerId', 'lang']
                //     });
                //     groupusers.remove(n.userId)
                //     registeredUsers.push(groupusers)
                //     registeredUsers.forEach(element => {

                //         let lang = element["lang"]
                //         if (lang === n.lang) {
                //             n.sendNotification = true
                //         }
                //         else n.sendNotification = false
                //     });

                // }
                // //sadece grup etkiketliyse
                // // else if (n.groups.length > 0) {
                // //     console.log("GRUP");
                // //     let queryQuery = n.groups.map(function (elem) {
                // //         return { "groups": { "$all": [elem] } };
                // //     })
                // //     registeredUsers = await ctx.broker.call('registeredUsers.find', {
                // //         query: { "$or": queryQuery },
                // //         fields: ['_id', 'playerId', 'lang']
                // //     });

                // //     registeredUsers = registeredUsers.filter(x => x._id !== n.userId)

                // //     // registeredUsers.forEach(element => {
                // //     // 	if(element["lang"]===n.lang){
                // //     // 		n.sendNotification=true
                // //     // 	}
                // //     // 	else n.sendNotification=true
                // //     // });

                // // }

                // //sadece user etkiketliyse
                // else if (n.registeredUsers.length > 0) {
                //     console.log("KİŞİSEL");
                //     registeredUsers = await ctx.broker.call('registeredUsers.find', {
                //         query: { _id: { $in: n.registeredUsers } },
                //         fields: ['_id', 'playerId', 'lang']
                //     });
                //     registeredUsers.forEach(element => {
                //         if (element["lang"] === n.lang) {
                //             n.sendNotification = true
                //         }
                //         else n.sendNotification = false
                //     });
                // }

                // //sadece ikiside  etkiketli değilse herkese gönder
                // else {
                //     console.log("TUMU");

                //     registeredUsers = await ctx.broker.call('registeredUsers.find', {
                //         query: {},
                //         fields: ['_id', 'playerId', 'lang']
                //     });
                // }

                if (n.registeredUsers.length > 0) {
                    registeredUsers = await ctx.broker.call('registeredUsers.find', {
                        query: { _id: { $in: n.registeredUsers } },
                        fields: ['_id', 'playerId', 'lang']
                    });
                    registeredUsers.forEach(element => {
                        if (element["lang"] === n.lang) {
                            n.sendNotification = true
                        }
                        else n.sendNotification = false
                    });
                }
                if (n.area.length > 0) {
                    registeredUsers = await ctx.broker.call('registeredUsers.find', {
                        query: {},
                        fields: ['_id', 'playerId', 'lang','muhatapNo']
                    });

                    let  bolge = await this.broker.call("location.find", {
                        query: {
                            _id:{$in:n.area.map((x) => x)}
                          },	  							
                      });
                      let subscribers=[]

                    if (n.district.length > 0) {
                        let ilce=bolge.map((x)=>{
                            let [ilce] = x.district.filter(a => n.district.some(b => a._id === b));  
                            return ilce
                        })
                        if (n.neighbourhood.length > 0) {
                            let mahalle=ilce.map((x)=>{
                                let [mahalle] = x.neighbourhood.filter(a => n.neighbourhood.some(b => a._id === b));  
                                return mahalle
                            })
                            await Promise.all(
                                await registeredUsers.map(async(x)=>{
                                    let user = await ctx.broker.call("subscriber.find", {
                                        query: {
                                          MUHATAP_NO: x.muhatapNo,
                                          BOLGE:{$in:bolge.map((x) => x.area)},
                                          ILCE:{$in:ilce.map((x) => x.name)},
                                          MAHALLE:{$in:mahalle.map((x) => x.name)},
                                        },
                                      });
                                    if(user.length>0){
                                      subscribers.push(x)
                                      return x
                                    }
                                })
                            );
                            
                        }
                        else{
                            
                            await Promise.all(
                                await registeredUsers.map(async(x)=>{
                                    let user = await ctx.broker.call("subscriber.find", {
                                        query: {
                                          MUHATAP_NO: x.muhatapNo,
                                          BOLGE:{$in:bolge.map((x) => x.area)},
                                          ILCE:{$in:ilce.map((x) => x.name)}
                                        },
                                      });
                                    if(user.length>0){
                                      subscribers.push(x)
                                      return x
                                    }
                                })
                            );
                            
                        }
                    }
                    else{	
                        await Promise.all(
                            await registeredUsers.map(async(x)=>{
                                let user = await ctx.broker.call("subscriber.find", {
                                    query: {
                                    MUHATAP_NO: x.muhatapNo,
                                    BOLGE:{$in:bolge.map((x) => x.area)}
                                    },
                                });
                                if(user.length>0){
                                subscribers.push(x)
                                return x
                                }
                            }));
                        }

                    registeredUsers=subscribers

                    registeredUsers.forEach(element => {
                        if (element["lang"] === n.lang) {
                            n.sendNotification = true
                        }
                        else n.sendNotification = false
                    });
                     
                }
                else if(!(n.muhatapNo=="" && n.muhatapNo==null)) {
                    registeredUsers = await ctx.broker.call('registeredUsers.find', {
                        query:  {muhatapNo:n.muhatapNo},
                        fields: ['_id', 'playerId', 'lang','muhatapNo']
                    });
                    registeredUsers.forEach(element => {
                        if (element["lang"] === n.lang) {
                            n.sendNotification = true
                        }
                        else n.sendNotification = false
                    });
                }
                else{
                    console.log("TUMU");

                    registeredUsers = await ctx.broker.call('registeredUsers.find', {
                        query: {},
                        fields: ['_id', 'playerId', 'lang']
                    });
                }
                //burda aynı dil olmayanlar elenecek
                let hasPlayerIds = registeredUsers.filter(x => x.playerId != null && x.playerId.length > 0 && x.lang == n.lang);

                try {

                	this.broker.call('notifications.insert', {
							entities: registeredUsers.map(a => ({
								isRead: false,
								registeredUserId: a._id,
								title: n.title,
								active: n.active,
								isIndividual: n.isIndividual,
								sendNotification: n.sendNotification,
								pushNotificationId: n._id,
								item_id: n.item_id,
								content: n.content,
								date: n.date,
								icon: n.icon,
							//	media: n.media,
							//	type: n.type,
							//	groups: n.groups || [],
								area: n.area || [],
								district: n.district || [],
								neighbourhood: n.neighbourhood || [],
								lang: n.lang || 'TR',
								subtype: n.subtype || ''
							}))
						});
                    console.log("BİLDİRİM:", n.sendNotification + " - " + hasPlayerIds.length + " Kişi")
                    if (hasPlayerIds.length > 0 && n.sendNotification) {
                        let result = await axios.post('https://onesignal.com/api/v1/notifications', {
                            'app_id': process.env.ONESIGNAL_APPID,
                            'include_player_ids': hasPlayerIds.map(x => x.playerId),
                            'data': {
                                'title': n.title,
                                'item_id': n.item_id,
                                'subtype': n.subtype
                            },
                            'contents': { 'en': n.content, 'tr': n.content },
                            'headings': { 'en': n.title, 'tr': n.title },
                        }, {
                            headers: {
                                'Authorization': 'Basic ' + process.env.ONESIGNAL_APIKEY
                            }
                        });
                    }

                }
                catch (err) {
                    this.logger.error(err);
                }
            });
		},
	},

};