'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
let nanoid = require('nanoid');

let icon =  {
	'_id': 'notification.png',
	'url': process.env.CDN + '/general/notification.png',
	'thumb': process.env.CDN + '/general/notification.png',
	'type': 'image',
	'mimeType': 'image/png'
}

module.exports = {
	name: 'notifications',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'notifications',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			title: {type: 'string'},
			active: 'boolean',
			isIndividual: 'boolean',
			muhatapNo:{ type: 'string', optional: true },
			sendNotification: {type: 'boolean', optional: true},
			pushNotificationId: {type: 'string', optional: true },
			item_id: {type: 'string', optional: true},
			content: 'string',
			date: 'date',
			created: 'date',
			lang:{ type:'string', optional:true },
		//	groups:{ type:'array', items:'string', optional:true },
		registeredUserId: {type: 'string', optional: true },
			area: [{ type: 'array', items: 'string', optional: true }],
			district: [{ type: 'array', items: 'string', optional: true }],
			neighbourhood: [{ type: 'array', items: 'string', optional: true }],
		//	type: {type: 'string',enum: ['not_modul', 'notifications', 'pushNotifications', 'survey', 'banners', 'user']},
			subtype: {type: 'string', optional: true},
			isRead: 'boolean',
			icon: {'type': 'object', props: {
				type: {type:'string'},
				thumb: {type:'string'},
				url: {type:'string'},
				_id: {type:'string'},
				mimeType: {type:'string'}
			}},
			// media: {'type': 'object', props: {
			// 	type: {type:'string'},
			// 	thumb: {type:'string'},
			// 	url: {type:'string'},
			// 	_id: {type:'string'},
			// 	mimeType: {type:'string'}
			// }},
			readUsers: {type: 'array', optional:true, items: {type: 'object', props: {
				id: 'string',
				date: 'date'
			}}}
		},
		populates: {
			"userId": {
				action: "registeredUsers.get",
				params: {
					fields: ["name","lastname","email","phone"]
				}
			}
		},
	},

	actions: {
		updateAll: {
			handler(ctx){
				this.adapter.updateMany({pushNotificationId: ctx.params.pushNotificationId}, {
					$set: {
						...ctx.params
					}
				})
			}
		},
		async itemsRemove (ctx) {
			let item_id = ctx.params.item_id;

			let many = await this.adapter.removeMany({
				item_id: item_id
			});

		},
		async pushNotificationRemove (ctx) {
			let pushNotificationId = ctx.params.pushNotificationId;

			let many = await this.adapter.removeMany({
				pushNotificationId: pushNotificationId
			});

		}
	},

	hooks: {
		before: {
			insert: [ctx => {
				ctx.params.entities.map(e => {
					e._id = nanoid(25);
					e.created = new Date();
					e.isRead = false;
				//	e.type = e.type  || 'notification';
					e.icon = e.icon || icon;
				//	e.media = e.media || icon;
				//	e.groups = e.groups || [],
				e.area = e.area || [],
				e.district = e.district || [],
				e.neighbourhood = e.neighbourhood || [],
					e.lang= e.lang || 'TR',
					e.subtype = e.subtype || ''
				});
			}],
			create: [ctx => {
				ctx.params._id = nanoid(25);
				ctx.params.created = new Date();
				ctx.params.isRead = false;
				//ctx.params.type = ctx.params.type  || 'notification';
				ctx.params.icon = ctx.params.icon || icon;
			//	ctx.params.media = ctx.params.media || icon 
				ctx.params.subtype = ctx.params.subtype || ''
			}]
		}
	}
};