'use strict';
const flexsearch = require('flexsearch');
const MongoClient = require('mongodb').MongoClient;
const uri = process.env.MONGO_URL;

flexsearch.registerMatcher({
	"ü": "u", "ş": "s", "ı": "i", "ö":"o", "ğ":"g", "ü":"u"
})


module.exports = {
	name: 'search',

	actions: {
		async postSearch(ctx){
			if(ctx.params.text)
			return this.posts ? this.posts.search({
				query: ctx.params.text.toLocaleLowerCase('tr'),
				suggest: true
			}) : [];
			else return []
		},

		async eventSearch(ctx){
			if(ctx.params.text)
				return this.events ? this.events.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async campaingSearch(ctx){
			if(ctx.params.text)
				return this.campaings ? this.campaings.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},
		async coordinateSearch(ctx){
			if(ctx.params.text){
				let a=this.coordinates ? this.coordinates.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
				return a;
			}

			else return []
		},
		async registeredUserSearch(ctx){
			if(ctx.params.text){
				let a=this.registeredUsers ? this.registeredUsers.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
				return a;
			}
			else return []
		},
		async bannerSearch(ctx){
			if(ctx.params.text){
			let a=this.banners ? this.banners.search(ctx.params.text.toLocaleLowerCase('tr')) : []
				return a;			
			}
			else return []
		},
		async surveySearch(ctx){
			if(ctx.params.text){
			let a=this.surveys ? this.surveys.search(ctx.params.text.toLocaleLowerCase('tr')) : []
				return a;			
			}
			else return []
		},
		async requestAndComplaintSearch(ctx){
			if(ctx.params.text){
			let a=this.requestAndComplaint ? this.requestAndComplaint.search(ctx.params.text.toLocaleLowerCase('tr')) : []
				return a;			
			}
			else return []
		},
		async pushNotificationSearch(ctx){
			if(ctx.params.text){
			let a=this.pushNotifications ? this.pushNotifications.search(ctx.params.text.toLocaleLowerCase('tr')) : []
				return a;			
			}
			else return []
		},
		async search (ctx) {
			if(ctx.params.text){
			let txt = ctx.params.text.toLocaleLowerCase('tr');
			let posts = this.posts ? this.posts.search(txt, 5) : [];
			let banners = this.banners ? this.banners.search(txt, 5) : [];
			let surveys = this.surveys ? this.surveys.search(txt, 5) : [];
			let requestAndComplaint = this.requestAndComplaint ? this.requestAndComplaint.search(txt, 5) : [];
			let pushNotifications = this.pushNotifications ? this.pushNotifications.search(txt, 5) : [];
			let events = this.events ? this.events.search(txt, 5) : [];
			let news = this.news ? this.news.search(ctx.params.text, 5) : [];
			let coordinates = this.coordinates ? this.coordinates.search(txt, 5) : [];
			let registeredUsers = this.registeredUsers ? this.registeredUsers.search(txt, 5) : [];
			let campaings = this.campaings ? this.campaings.search(txt, 5) : [];

			return [...posts, ...events, ...news, ...coordinates, ...campaings, ...banners, ...surveys, ...registeredUsers, ...pushNotifications, ...requestAndComplaint];
			}
			else return []
		},

		async reindex (ctx){
			let client = this.client;
			
			this.indexPosts(client);
			this.indexEvents(client);
			this.indexNews(client);
			this.indexCoordinates(client);
			this.indexRegisteredUsers(client);
			this.indexBanners(client);
			this.indexSurveys(client);
			this.indexRequestAndComplaint(client);
			this.indexPushNotifications(client);
			this.indexCampaings(client);
		},

		async reindexCoordinates(){
			this.indexCoordinates(this.client);
		},
		async reindexBanners(){
			this.indexBanners(this.client);
		},
		async reindexRegisteredUsers(){
			this.indexRegisteredUsers(this.client);
		},
		async reindexSurveys(){
			this.indexSurveys(this.client);
		},
		async reindexRequestAndComplaint(){
			this.indexRequestAndComplaint(this.client);
		},
		async reindexPushNotifications(){
			this.indexPushNotifications(this.client);
		},
	},

	methods: {
		async indexPosts(client){
			let posts = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items =  await client.db(process.env.DB)
				.collection('posts').find({
					active: true,
					date: {$lte: new Date()}
				})
				.project({ comment: 1 , coordinate: 1})
				.toArray();

			posts.add(items.map(x => ({
				_id: x._id,
				tid: 'postwall::' + x._id,
				type: 'postwall',
				title: (x.comment || '').toLocaleLowerCase('tr'), 
				content: (x.coordinate ? (x.coordinate.address || '') : '').toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/postwall.png'
			})));
	
			this.posts = posts;
		},
		
		async indexEvents(client){
			let events = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('events')
				.find({})
				.project({ name: 1, description: 1, content: 1, coordinate: 1 })
				.toArray();

			events.add(items.map(x => ({
				_id: x._id,
				tid: 'events::' + x._id,
				type: 'events',
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: (x.description + ' ' + x.content + (x.coordinate && x.coordinate.address ? x.coordinate.address : '')).toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/events.png'
			})));

			this.events = events;
		},

		async indexNews(client){
			let news = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('news').find({})
				.project({ name: 1 })
				.toArray();

			news.add(items.map(x => ({
				_id: x._id,
				tid: 'news::' + x._id,
				type: 'news',
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/postwall.png'
			})));

			this.news = news;
		},

		async indexCampaings(client){
			let campaings = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('campaings').find({
				active: true,
				startDate: {$lte: new Date()},
				endDate: {$gte: new Date()}
			})
				.project({ name: 1, sub_title: 1, cities: 1 })
				.toArray();

			let cities = await client.db(process.env.DB).collection('cities').find({}).toArray();

			items.forEach(x => {
				if(x.cities.map){
					x.cities = x.cities.map(cid => cities.find(city => city._id == cid))
						.filter(city => city != null)
						.map(city => city.name)
						.join(',');
				}
			});

			campaings.add(items.map(x => ({
				_id: x._id,
				tid: 'workfamily::' + x._id,
				type: 'workfamily',
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: (x.sub_title + ' ' + (x.cities || '')).toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/postwall.png'
			})));

			this.campaings = campaings;
		},

		async indexCoordinates(client){
			let coordinates = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB ).collection('coordinates').find({})
				.project({ name: 1, address: 1})
				.toArray();

			coordinates.add(items.map(x => ({
				_id: x._id,
				tid: 'health::' + x._id,
				type: 'health',
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/postwall.png'
			})));

			this.coordinates = coordinates;
		},
		async indexBanners(client){
			let banners = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});
	
			let items = await client.db(process.env.DB ).collection('banners').find({})
				.project({ title: 1})
				.toArray();
			banners.add(items.map(x => ({
				_id: x._id,
				tid: 'banners::' + x._id,
				type: 'banners',
				title: (x.title || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/postwall.png'
			})));
	
			this.banners = banners;
		},
		async indexSurveys(client){
			let surveys = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});
	
			let items = await client.db(process.env.DB ).collection('surveys').find({})
				.project({ name: 1})
				.toArray();
				surveys.add(items.map(x => ({
				_id: x._id,
				tid: 'surveys::' + x._id,
				type: 'surveys',
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/postwall.png'
			})));
	
			this.surveys = surveys;
		},
		async indexRequestAndComplaint(client){
			let requestAndComplaint = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});
	
			let items = await client.db(process.env.DB ).collection('requestAndComplaint').find({})
				.project({ message: 1})
				.toArray();
				requestAndComplaint.add(items.map(x => ({
				_id: x._id,
				tid: 'requestAndComplaint::' + x._id,
				type: 'requestAndComplaint',
				title: (x.message || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/postwall.png'
			})));
	
			this.requestAndComplaint = requestAndComplaint;
		},
		async indexPushNotifications(client){
			let pushNotifications = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});
	
			let items = await client.db(process.env.DB ).collection('pushNotifications').find({})
				.project({ title: 1})
				.toArray();
				pushNotifications.add(items.map(x => ({
				_id: x._id,
				tid: 'pushNotifications::' + x._id,
				type: 'pushNotifications',
				title: (x.title || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/postwall.png'
			})));
	
			this.pushNotifications = pushNotifications;
		},
		async indexRegisteredUsers(client){
			let registeredUsers = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});
	
			let items = await client.db(process.env.DB ).collection('registeredUsers').find({})
				.project({ name: 1,lastname:1,phone:1,email:1})
				.toArray();

				registeredUsers.add(items.map(x => ({
				_id: x._id,
				tid: 'registeredUsers::' + x._id,
				type: 'registeredUsers',
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: (x.lastname + ' ' + x.phone +' '+x.email).toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/postwall.png'
			})));
	
			this.registeredUsers = registeredUsers;
		}
	},
	
	async started(){
		const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

		await client.connect();
		this.client = client;

		this.indexPosts(client);
		this.indexEvents(client);
		this.indexNews(client);
		this.indexCoordinates(client);
		this.indexRegisteredUsers(client);
		this.indexBanners(client);
		this.indexSurveys(client);
		this.indexRequestAndComplaint(client);
		this.indexPushNotifications(client);
		this.indexCampaings(client);

	}
};

