'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
var sql = require("mssql");
var config = {
  user: process.env.USER,

  password: process.env.PASSWORD,

  server: process.env.SERVER,

  database: process.env.DATABASE,

  dialect: process.env.DIALECT,
  connectionTimeout: 600000,
  requestTimeout: 600000,
  pool: {
    idleTimeoutMillis: 600000,
    max: 100,
  },
  dialectOptions: {
    instanceName: "SQLEXPRESS",
  },

  options: {
    trustedConnection: true,
    encrypt: true,
    enableArithAbort: true,
    trustServerCertificate: true,
  },
};

module.exports = {
	name: 'location',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'location',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			area:{type: 'string'},
			district: {
				'type': 'array',
				items: {
					type: 'object',
					props: {	
						_id: {type: 'string'},
						name: 'string',
						neighbourhood: {
							type: 'array',
							items: {
								type: 'object',
								props: {	
									_id: {type: 'string'},
									name: 'string'
								}
							}
						}
					}
				}
			}
		}	
	},
	actions: {
        async getLocation(ctx){
			let pool = await sql.connect(config);
			const request = pool.request();
			let today="abone_"+ctx.params.today
			let bolge = await request.query("select	DISTINCT BOLGE from "+today);
			let array=[]
			const partial_result = await request.query("select	DISTINCT BOLGE, ILCE, MAHALLE from "+ today +" GROUP BY BOLGE,ILCE,MAHALLE");
			
		await bolge.recordset.map(async (x)=>{
			let ilcee=[]
				let myArray=partial_result.recordset.filter(z => z.BOLGE ==x.BOLGE)
				let ilce =[...new Set(myArray.map(item => item.ILCE))];

				await ilce.map(async (y)=>{
					
				let mah=[]
					let myArray=partial_result.recordset.filter(z => z.BOLGE ==x.BOLGE && z.ILCE ==y)
					let mahalle =[...new Set(myArray.map(item => item.MAHALLE))];
					await mahalle.map((a)=>{
							let json={
									"_id" : a.toLocaleLowerCase('en-US').replace(/ /g,''),
									"name" : a
							}
							mah.push(json)
						})

						let json2={
							"_id" : y.toLocaleLowerCase('en-US').replace(/ /g,''),
							"name" : y,
							"neighbourhood" : mah
					}
					ilcee.push(json2)						
				})
					let json3={
						"_id" :(x.BOLGE).toLocaleLowerCase('en-US').replace(/ /g,''),
						"area" : x.BOLGE,
						"district" : ilcee
				}
				array.push(json3)
		}) 
			if(array.length>0){
			await this.adapter.removeMany({});}
		
		 	let a =await this.adapter.insertMany(array);
			return a.length;

        },
    }
};