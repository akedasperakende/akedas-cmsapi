'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const dayjs = require('dayjs');
const nanoid = require('nanoid');
const axios = require('axios')
module.exports = {
	name: 'requestAndComplaintCategories',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL,
		{ useNewUrlParser: true, useUnifiedTopology: true }, process.env.DB),
	collection: 'requestAndComplaintCategories',
	settings: {
		entityValidator: {
			$$strict: true,
			_id: { type: 'string' },
			name: { type: 'string', optional: true },
			lang:{type: 'string', optional: true},
			active: 'string',
			order: 'number',
			createdAt: 'date',
			updatedAt: 'date',

		}

	},
	hooks: {
		before: {
			create: ctx => {
				let x = ctx.params
				x._id = nanoid(25);
				x.createdAt = dayjs().toDate();
				x.updatedAt = dayjs().toDate();
				if (x.active == true) x.active = "1"
				else x.active = "0"
			},
			remove: async ctx => {
				let result = await ctx.call('requestAndComplaintSubCategories.find',{query:{
					categoryId:ctx.params.id
				}});
				if (result.length > 0) {
					throw Error("Alt Kategorisi Bulanan Bir Kategoriyi Silemezsiniz")
				}
				return result
			},
			update: ctx => {
				let x = ctx.params
				x.updatedAt = dayjs().toDate();
				x.updatedBy = ctx.meta.user._id

				if (x.active == true) x.active = "1"
				else x.active = "0"
			},

		},
		after:{

		}
	}

};
