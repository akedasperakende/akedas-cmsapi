'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const nanoid = require('nanoid');
const dayjs = require('dayjs');

module.exports = {
	name: 'settings',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'settings',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'}
		}
    },

    hooks: {
		before: {
			list: [(ctx) => {
				ctx.params.pageSize = 50;
            }],
            update:[(ctx)=> {
                ctx.params.updatedBy = ctx.meta.user._id;
            }]

		}
	}
};