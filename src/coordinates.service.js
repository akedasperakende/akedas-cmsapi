'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const fs = require("fs")
const nanoid = require('nanoid');
const { error } = require('console');

module.exports = {
	name: 'coordinates',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL,
		{useNewUrlParser: true, useUnifiedTopology: true}, process.env.DB),
	collection: 'coordinates',

	async started() {
		// let coordinates = fs.readFileSync('./coordinates.json')
		// coordinates = JSON.parse(coordinates)
		// coordinates.forEach(c => {
		// 	if(c.type == 'Eczane')
		// 		c.name += " ECZANESİ"
		// 	c.content = `<p>Adres: ${c.address}<p/><br><a href="tel:+90${c.phone}">Tel: ${c.phone}</>`
		// })

		// await this.adapter.insertMany(coordinates)
		// console.log("completed")
	},

	hooks: {
		before: {
			list: async ctx => {
				let search = ctx.params.search
				if (search && search.length > 2) {
					let result = await ctx.broker.call('search.coordinateSearch', {text: search})

					if (!ctx.params.query)
						ctx.params.query = {}
					ctx.params.query._id = {$in: result.map(r => r._id)}
				}
				delete ctx.params.search
			},
			create: ctx => {
				if(!ctx.params.area){
					throw Error("Lütfen Bölge Seçiniz!");
				}
				let c = ctx.params
				c._id = nanoid(16)
				c.code = c._id
				c.id = c._id

				if (c.coordinate) {
					c.address = c.coordinate.address
					if (c.coordinate.position) {
						c.lat = c.coordinate.position.lat.toString()
						c.lon = c.coordinate.position.lng.toString()
					}
				}
				// c.content = `<p>Adres: ${c.address || ''}<p/><br><a href="tel:${c.phone}">Tel: ${c.phone}</>`
				c.color = '#1e90ff'

				c.search = c.city + ' ' + c.district + ' ' + c.address + ' ' + c.locality
			},
			update: ctx => {
				let c = ctx.params
				if (c.coordinate) {
					c.address = c.coordinate.address
					if (c.coordinate.position) {
						c.lat = c.coordinate.position.lat.toString()
						c.lon = c.coordinate.position.lng.toString()
					}
				}
				c.color = '#1e90ff'
				// c.content = `<p style="white-space: pre-wrap">Adres: ${c.address || ''}<p/><br><a href="tel:${c.phone}">Tel: ${c.phone}</>`
				c.search = c.city + ' ' + c.district + ' ' + c.address + ' ' + c.locality
			}
		},
		after: {
			list: (ctx, result) => {
				result.rows.forEach(c => {
					if (!c.coordinate) {
						c.locality = c.locality || ''
						c.coordinate = {
							address: c.address,
							position: {
								lat: Number(c.lat),
								lng: Number(c.lon)
							}
						}
					}
				})

				return result
			},

			create: (ctx, result) => {
				ctx.call('search.reindexCoordinates', {})
				return result
			}
		}
	}
};
