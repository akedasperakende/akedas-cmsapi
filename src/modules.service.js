'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');

module.exports = {
	name: 'modules',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'modules',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			name: 'string',
			icon: 'string',
			order: 'number',
			type: {type: 'string', enum: ['postwall', 'survey', 'events', 'support', 'workfamily', 'news', 'users', 'wordgames', 'education', 'infos']}
		}
	},
	hooks: {
		before: {
			list: [(ctx) => {

				ctx.params.query = {}

				if(ctx.params.lang)
					ctx.params.query.lang  = ctx.params.lang

				if(ctx.params.adminShow){
					if(ctx.params.adminShow == "true")
						ctx.params.query.adminShow  = true
					else
						ctx.params.query.adminShow  = false
				}

				if(ctx.params.appShow){
					if(ctx.params.appShow == "true")
						ctx.params.query.appShow  = true
					else
						ctx.params.query.appShow  = false
				}
					
				ctx.params.sort = "order"
				ctx.params.pageSize = 100;
			}]

		}
	}
};