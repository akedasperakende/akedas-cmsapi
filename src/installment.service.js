"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
let nanoid = require("nanoid");
const dayjs = require('dayjs');
var sql = require("mssql");

var config = {
  user: process.env.USER,
  password: process.env.PASSWORD,
  server: process.env.SERVER,
  database: process.env.DATABASE,
  dialect: process.env.DIALECT,
  connectionTimeout: 600000,
  requestTimeout: 600000,
  pool: {
    idleTimeoutMillis: 600000,
    max: 100,
  },
  dialectOptions: {
    instanceName: "SQLEXPRESS",
  },
  options: {
    trustedConnection: true,
    encrypt: true,
    enableArithAbort: true,
    trustServerCertificate: true,
  },
};
module.exports = {
  name: "installment",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "taksit",
  settings: {
    maxPageSize: 1000000,
  },
  actions: {
    async getmove() {
      let pool = await sql.connect(config);
      const request = pool.request();
      let installmenttable = await this.broker.call("installment.count");
      const result = await request.query("select * from taksit");
      let record; 
      if (installmenttable > 0 && result.recordset.length > 0) {
        await this.adapter.removeMany({});
        let installment = await this.broker.call("installment.count");
        if (installment == 0) {
          record = await result.recordset.map((r) => {
            (r._id = nanoid(25)),
            (r.SOZLESME_HESAP_NO= r.SOZLESME_HESAP_NO== null ? null: String(r.SOZLESME_HESAP_NO)),
            (r.TUTAR = r.TUTAR== null ? null:Number(r.TUTAR)),
            (r.BELGE_NO = r.BELGE_NO== null ? null:String(r.BELGE_NO)),
            (r.SON_ODEME = r.SON_ODEME== null ? null:dayjs(r.SON_ODEME).toDate()),
            (r.ODEME_DURUM = r.ODEME_DURUM== null ? null:String(r.ODEME_DURUM)),
            (r.TAKSIT_SIRA = r.TAKSIT_SIRA== null ? null:Number(r.TAKSIT_SIRA));
          return r;
        });
        await this.adapter.insertMany(record);
          return true;
        }
      }

      if (installmenttable == 0 && result.recordset.length > 0) {
        record = await result.recordset.map((r) => {
          (r._id = nanoid(25)),
          (r.SOZLESME_HESAP_NO= r.SOZLESME_HESAP_NO== null ? null: String(r.SOZLESME_HESAP_NO)),
          (r.TUTAR = r.TUTAR== null ? null:Number(r.TUTAR)),
          (r.BELGE_NO = r.BELGE_NO== null ? null:String(r.BELGE_NO)),
          (r.SON_ODEME = r.SON_ODEME== null ? null:dayjs(r.SON_ODEME).toDate()),
          (r.ODEME_DURUM = r.ODEME_DURUM== null ? null:String(r.ODEME_DURUM)),
          (r.TAKSIT_SIRA = r.TAKSIT_SIRA== null ? null:Number(r.TAKSIT_SIRA));
        return r;
      });
      await this.adapter.insertMany(record);
        return true;
      }
    },

    async changeInstallment(ctx) {

      let pool = await sql.connect(config);
      const request = pool.request();

      let today="taksit_"+ctx.params.today
      let yesterday="taksit_"+ctx.params.yesterday
      let month=ctx.params.month
           const partial_result = await request.query(
       " select * from "+today+"  where [AY] > "+month+" except  select * from "+yesterday+"  where [AY] > "+month);
 
    let record = await partial_result.recordset.map((r) => {
        (r._id = nanoid(25)),
        (r.SOZLESME_HESAP_NO= r.SOZLESME_HESAP_NO== null ? null: String(r.SOZLESME_HESAP_NO)),
        (r.TUTAR = r.TUTAR== null ? null:Number(r.TUTAR)),
        (r.BELGE_NO = r.BELGE_NO== null ? null:String(r.BELGE_NO)),
        (r.SON_ODEME = r.SON_ODEME== null ? null:dayjs(r.SON_ODEME).toDate()),
        (r.ODEME_DURUM = r.ODEME_DURUM== null ? null:String(r.ODEME_DURUM)),
        (r.TAKSIT_SIRA = r.TAKSIT_SIRA== null ? null:Number(r.TAKSIT_SIRA));
        return r;
      });
      let addData= await this.adapter.insertMany(record);
      return addData.length
    },
    async deleteInstallment(ctx) {

      let pool = await sql.connect(config);
      const request = pool.request();
      let today="taksit_"+ctx.params.today
      let yesterday="taksit_"+ctx.params.yesterday
      let month=ctx.params.month
      const partial_result = await request.query(
       " select * from "+today+"  where [AY] > "+month+" except  select * from "+yesterday+"  where [AY] > "+month);

      let rem = await this.adapter.removeMany({
        SOZLESME_HESAP_NO: {
           $in: partial_result.recordset.map((x) => x.SOZLESME_HESAP_NO)
      }
      });

      return rem
    },
  },
};
