'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const dayjs = require('dayjs');
const nanoid = require('nanoid');

module.exports = {
	name: 'banners',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'banners',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			lang:{type: 'string', optional: true},
			title:{type: 'string', optional: true},
			order: {type: 'number'},
			active: 'boolean',
			startDate: 'date',
			endDate: 'date',
			isLink:'boolean',
			redirectLink:{type: 'string', optional: true},
			archive: {type: 'boolean', optional: true},
			registeredUsers:  [{type :'array', items: 'string', optional: true}],
			imageurl: 'string',
			type: {'type': 'string', optional: true},
			moduleId:{type: 'string', optional: true},
			item_id:{type: 'string', optional: true},
			media: {'type': 'object', optional: true, props: {
				$$strict: true,
				type: {type:'string'},
				thumb: {type:'string'},
				duration: {type: 'number', optional: true},
				url: {type:'string'},
				_id: {type:'string'},
				mimeType: {type:'string'},
				width: { type: 'number', optional: true },
				height: { type: 'number', optional: true },
			}},
			usersFile: {'type': 'object', optional: true, props: {
				type: {type:'string'},
				thumb: {type:'string'},
				url: {type:'string'},
				_id: {type:'string'},
				mimeType: {type:'string'}
			}},
			createdAt: 'date',
			createdBy: 'string',
			updatedAt: 'date',
			updatedBy: 'string'
		}
	},

	actions: {
		async archive() {
			this.adapter.updateMany({
				endDate: {$lte: new Date()}
			}, {
				$set : {
					archive: true
				}
			});
		}
	},

	hooks: {
		before: {
			list: async ctx => {
				let search = ctx.params.search
				if (search && search.length > 2) {
					let result = await ctx.broker.call('search.bannerSearch', {text: search})
					if (!ctx.params.query)
						ctx.params.query = {}
					ctx.params.query._id = {$in: result.map(r => r._id)}
				}
				delete ctx.params.search;
				let query = ctx.params.query ;
				
				if(query && query.archive === 'true'){
					query.archive = query && query.archive === 'true';
				}
				else if(query) query.archive = null;
			},
			create: [(ctx => {
				if(ctx.params.isLink==true){
					if(ctx.params.redirectLink==null || ctx.params.redirectLink==""){
						throw Error("Lütfen link alanını doldurunuz!");
					}
				}
				ctx.params._id = nanoid(25);
				ctx.params.registeredUsers = ctx.params.registeredUsers || [];
				ctx.params.redirectLink=ctx.params.redirectLink ? ctx.params.redirectLink : '';
				ctx.params.imageurl = ctx.params.media ? ctx.params.media.url: '';
				if(!ctx.params.imageurl){
					throw Error("Dosya yüklerken bir sorun oluştu!");
				}
				let type = ctx.params.moduleId
				if(type == "pushNotifications"){
					ctx.params.type = 'notifications'
					ctx.params.moduleId = 'notifications'
				}
				if(type === 'infos2' || type === 'infos3' || type === 'infos4' || type === 'infos5'){
					ctx.params.moduleId = type
					ctx.params.type = 'infos'
				}else ctx.params.type =type
				ctx.params.startDate = dayjs(ctx.params.startDate).toDate();
				ctx.params.endDate = dayjs(ctx.params.endDate).toDate();
				ctx.params.createdAt = dayjs().toDate();
				ctx.params.createdBy = ctx.meta.user._id;
				ctx.params.updatedAt = dayjs().toDate();
				ctx.params.updatedBy = ctx.meta.user._id;
				
			})],

			update: [(ctx => {
				if(ctx.params.isLink==true){
					if(ctx.params.redirectLink==null || ctx.params.redirectLink==""){
						throw Error("Lütfen link alanını doldurunuz!");
					}
				}
				ctx.params.imageurl = ctx.params.media ? ctx.params.media.url : '';
				ctx.params.redirectLink=ctx.params.redirectLink ? ctx.params.redirectLink : '';
				ctx.params.registeredUsers = ctx.params.registeredUsers || [];
				let module = ctx.params.moduleId
				if(module === 'infos2' || module === 'infos3' || module === 'infos4' || module === 'infos5'){
					ctx.params.type = 'infos'
					ctx.params.moduleId = module
				}
				else{
					ctx.params.type = module
				}
				
				if(ctx.params.type == "pushNotifications"){
					ctx.params.type = 'notifications'
					ctx.params.moduleId = 'notifications'
				}
				ctx.params.startDate = dayjs(ctx.params.startDate).toDate();
				ctx.params.endDate = dayjs(ctx.params.endDate).toDate();
				delete ctx.params.createdAt;
				delete ctx.params.createdBy;
				ctx.params.updatedAt = dayjs().toDate();
				ctx.params.updatedBy = ctx.meta.user._id;
			})]
		}
	}
};