'use strict';
const path = require('path');
const jwt = require('jsonwebtoken');
const MongoClient = require('mongodb').MongoClient;
const dayjs = require('dayjs');
const fastifyPlugin = require('fastify-plugin');
let qs = require('qs')
const dev = process.env.NODE_ENV !== 'production'
let cmsIPS = process.env.IPS ? process.env.IPS.split(',') : []
const helmet = require('fastify-helmet');
// dev test 7
module.exports = {
	name: 'api',

	settings: {
		port: process.env.PORT || 5000
	},

	created() {

		try {
			const uri = process.env.MONGO_URL || 'mongodb://localhost:27017/admin';
			const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
			let db = null;
			client.connect().then(() => {
				db = client.db(process.env.DB);
			});

			let fastify = require('fastify')({
				pluginTimeout: process.env.NODE_ENV === 'development' ? 200000 : 200000,
				caseSensitive: false,
				trustProxy: true,
				logger: false
			});
			this.fastify = fastify;

			fastify.register(
				helmet,
				{ hidePoweredBy: { setTo: 'PHP 4.2.0' } }
			)

			fastify.register(fastifyPlugin(async (fastify) => {
				let mongo = await MongoClient.connect(
					process.env.MONGO_URL,
					{
						useUnifiedTopology: true
					});
				let db = mongo.db(process.env.DB);
				fastify.decorate('db', db);
			}));

			fastify.register(require('fastify-cors'), {
				origin: true,
				methods: 'GET,PUT,POST,PATCH,DELETE',
				preflight: true
			});

			fastify.register(require('fastify-multipart'), {
				addToBody: true,
				limits: {
					fieldNameSize: 100, // Max field name size in bytes
					fieldSize: 53000000, // Max field value size in bytes
					fields: 20,         // Max number of non-file fields
					fileSize: 53000000,      // For multipart forms, the max file size
					files: 10,           // Max number of file fields
					headerPairs: 2000   // Max number of header key=>value pairs
				}
			});

			fastify.addHook('preHandler', async (req) => {
				let token = null;
				try {
					if (process.env.BLOCK_IP === "block" &&
						(req.req.url.startsWith("/cms")
							|| req.req.url.startsWith("/rest")) &&
						(req.ips && req.ips.find(ip => cmsIPS.find(_ip => _ip === ip)) == null)
					)
						throw Error("Forbidden")

					req.version =
						'v' + (req.headers['x-api-version'] || '0.0').split('.')[0];
					req.lang = req.headers['x-api-lang']
						? req.headers['x-api-lang']
						: 'EN';
					req.lang = req.headers["x-api-lang"] ? req.headers["x-api-lang"] : 'EN';


					if (req.query && req.query.token !== null)
						token = req.query.token;
					if (req.body && req.body.token != null) {
						token = req.body.token;
						delete req.body.token;
					}

					if (token == null && req.headers['authorization']) {
						token = req.headers['authorization'].replace('bearer ', '').replace('Bearer ', '');
					}
					if (token == null && req.headers['Authorization']) {
						token = req.headers['Authorization'].replace('bearer ', '').replace('Bearer ', '');
					}
					let user = null;

					if (token) {
						try{
						user = jwt.verify(token, process.env.JWT_SECRET);
						req.user = user;
						}
						catch(err){
							console.log("Hata");
							throw Error("Token süresi doldu")
							
						}
					
					}
					if (req.req.url.startsWith('/api/Login')) return;
					if (req.req.url.startsWith('/api/login')) return;
					if (req.req.url.startsWith('/api/register')) return;
					if (req.req.url.startsWith('/api/Register')) return;
					if (req.req.url.startsWith('/api/settings')) return;
					if (req.req.url.startsWith('/api/Settings')) return;
					if (req.req.url.startsWith('/api/forgotPassword')) return;
					if (req.req.url.startsWith('/api/smsCheckPass')) return;
					if (req.req.url.startsWith("/api/bill")) return;
					if (req.req.url.startsWith("/api/deletebill")) return;
					if (req.req.url.startsWith("/api/installment")) return;
					if (req.req.url.startsWith("/api/deletingOldDataBill")) return;
					if (req.req.url.startsWith("/api/subscribers")) return;
					if (req.req.url.startsWith("/api/location")) return;
					if (req.req.url.startsWith("/api/deletesubscriber")) return;

					if (!(req.req.url.startsWith('/api') ||
						req.req.url.startsWith('/rest'))) return;

					// if(token){
					// 	let _token = await this.broker.call('login.get', {id: user.tokenId });

					// 	if(_token == null)
					// 		throw new Error('Yetkisiz İşlem');
					// }
					if (!token) {

						let resultMessage = await this.broker.call('resultMessage.get', { id: '0013' })
						throw new Error(resultMessage.message)
						//throw new Error('Yetkisiz İşlem');
					}

					if (user == null || user._id == null) {
						let resultMessage = await this.broker.call('resultMessage.get', { id: '0020' })
						throw new Error(resultMessage.message)
						//throw new Error('Geçersiz Token');
					}

					let isExpire = await this.broker.call('registeredUsers.isExpire', {}, { meta: { user: user } })
					if (isExpire) {
						let resultMessage = await this.broker.call('resultMessage.get', { id: '0013' })
						throw new Error(resultMessage.message)
						//	throw new Error('Yetkisiz İşlem')
					}
					if (req.req.url.startsWith('/rest') &&
						!(user.role === "admin" || user.role === 'health' || user.role === "superAdmin")) {
						let resultMessage = await this.broker.call('resultMessage.get', { id: '0013' })
						throw new Error(resultMessage.message)
					}
				} catch (err) {
					this.logger.error(req.req.method)
					this.logger.error(req.req.url)
					this.logger.error(token)
					this.logger.error(req.body)
					this.logger.error(err);

					err.statusCode = 401;
					throw err;
				}
			});

			fastify.addHook('preSerialization', async (req, res, payload) => {

				if (payload && payload.result_message) {
					return payload;
				}
				else return {
					result: payload,
					result_message: {
						type: 'success',
						title: 'Bilgi',
						message: 'Başarılı'
					}
				};

			});

			fastify.setErrorHandler((error, request, reply) => {
				this.logger.error(error);
				if (error.statusCode == 401)
					reply.code(401)
						.send({
							result: null,
							result_message: {
								type: 'token_expire',
								title: 'Hata',
								message: error.message
							}
						});
				else reply.code(200)
					.send({
						result: null,
						result_message: {
							type: 'error',
							title: 'Hata',
							message: (error.data && Array.isArray(error.data)) ? error.data.reduce((m, x) => m + ' ' + x.message, error.message) : error.message
						}
					});
			});

			fastify.post('/api/search', async (req) => this.broker.call('search.search', { text: req.body.text }));

			fastify.post('/api/userSearch', async (req) => this.broker.call('registeredUsers.search', { text: req.body.text }));

			fastify.post('/api/login', async (req) => this.broker.call('login.token', { ...req.body }));
			fastify.post('/api/register', async (req) => this.broker.call('registeredUsers.create', { ...req.body }));
			fastify.post('/api/multipleRegister', async (req) => {
				if (req.user.role === 'superAdmin')
					return this.broker.call('registeredUsers.multipleCreate', { ...req.body }, { meta: { user: req.user } })
			});

			fastify.post('/api/multipleDeltaUser', async (req) => {
				if (req.user.role === 'superAdmin')
					return this.broker.call('registeredUsers.deltaUsersUpdate', { ...req.body }, { meta: { user: req.user } })
			});


			fastify.post('/api/logout', async (req) => this.broker.call('login.expire', {}, { meta: { user: req.user } }));

			fastify.get('/api/settings', async (req) => {
				return this.broker.call(req.version + '.settings.flat', {
					lang: req.lang,
					userId: req.user ? req.user._id : null,
				});
			});

			fastify.post('/api/forgotPassword', async (req) => await this.broker.call('registeredUsers.forgotPassword', { ...req.body }));
			fastify.post('/api/smsCheckPass', async (req) => this.broker.call('login.smsCheckPassword', { ...req.body }));

			fastify.get('/api/infos', async (req) => this.broker.call('infos.find', { query: { module: req.query.module || 'infos' } }));

			fastify.get('/api/registeredUsers', async () => this.broker.call('registeredUsers.find', { page: 1, pageSize: 20 }));
			fastify.get('/api/usersActiveCount', async (req) => this.broker.call('registeredUsers.count', {...req.query}));

			fastify.get('/api/registeredUsers/:id', async (req) => this.broker.call('registeredUsers.get', { id: req.params.id || req.user._id }));
			fastify.get('/api/me', async (req) => {
				let me = await this.broker.call('registeredUsers.get',
					{
						id: req.user._id,
						playerId: req.headers['x-player_id'] || req.query.player_id
					},
					{ meta: { user: req.user } });
				return me
			});
			fastify.post('/api/me', async (req) => {
				await this.broker.call('registeredUsers.update', { ...req.body, _id: req.user._id }, { meta: { user: req.user } });
				return true;
			});

			fastify.get('/api/me/educations', async (req) => ({
				lessons: await this.broker.call('lessons.todayLessons', {
					userId: req.user._id
				}),
				surveys: await this.broker.call('educationSurveys.find', {
					query: {
						userId: req.user._id,
						completed: false
					}
				})
			}));

			fastify.post('/api/me/education_joins', async (req) => {
				return await this.broker.call('lessons.join', {
					joinsLength: req.body.joinsLength,
					state: req.body.state,
					lessonId: req.body.lessonId,
					qrLessonId: req.body.qrLessonId,
					qr: req.body.qr,
					userId: req.user._id
				}, { meta: { user: req.user } });
			});

			fastify.get('/api/me/surveys/:id', async (req) => this.broker.call('educationSurveys.get', { id: req.params.id, completed: false }));
			fastify.post('/api/me/surveys/:id', async (req) => {
				await this.broker.call('educationSurveys.update',
					{ _id: req.params.id, completed: true, questions: req.body.questions });
				return true;
			});

			fastify.post('/api/birthday_message', async (req) => {
				await this.broker.call('birthdays.sendMessage', { ...req.body }, { meta: { user: req.user } });
				return true;
			});
			fastify.get('/api/stories', async () => await this.broker.call('stories.withBirthdays'));
			fastify.post('/api/stories', async (req) => {
				await this.broker.call('stories.create', { ...req.body, active: true, }, { meta: { user: req.user } });
				return true;
			});
			fastify.delete('/api/stories/:id/images/:img', async (req) => this.broker.call('stories.deleteImage', { ...req.body, story_id: req.params.id, img_id: req.params.img }));

			fastify.get('/api/posts',
				async (req) =>
					this.broker.call('posts.byUser',
						{
							limit: req.query.limit || req.query.increment,
							skip: req.query.skip || req.query.limit_min,
							search: req.query.search
						},
						{ meta: { user: req.user } })
			);

			fastify.get('/api/me/posts',
				async (req) =>
					this.broker.call('posts.mePosts',
						{
							limit: req.query.limit || req.query.increment,
							skip: req.query.skip || req.query.limit_min,
							search: req.query.search
						},
						{ meta: { user: req.user } })
			);

			fastify.get('/api/personalityInventory', async (req) => {
				return this.broker.call('personalityInventory.find', { ...req.body }, { user: req.user ? req.user._id : null })
			});

			fastify.post('/api/personalityInventory/:id', async (req) => {
				if (req.body.questions == null)
					throw Error('Kişilik testi null olamaz');

				let result = await this.broker.call('personalityInventoryAnswers.create', {
					personalityInventor_id: req.params.id,
					user_id: req.user._id,
					completed: true,
					questions: req.body.questions
				});
				return result;

			});
			fastify.get('/api/posts/:id', async (req) => this.broker.call('posts.getPost', { id: req.params.id }, { meta: { user: req.user } }));
			fastify.get('/api/posts/:id/likes', async (req) => this.broker.call('posts.likes', { post_id: req.params.id }, { meta: { user: req.user } }));
			fastify.post('/api/posts/:id/likes', async (req) => {
				await this.broker.call('posts.like', {
					post_id: req.params.id,
					...req.body
				},
					{ meta: { user: req.user } });
				return true;
			});
			fastify.delete('/api/posts/:id', async (req) => {
				await this.broker.call('posts.remove', { id: req.params.id }, { meta: { user: req.user } });
				return true;
			});
			fastify.post('/api/posts', async (req) => {

				let coordinate = (req.body.lat && req.body.lon)
					? {
						position: { lat: req.body.lat, lng: req.body.lon },
						address: req.body.address || ''
					}
					: null;
				await this.broker.call('posts.create', {
					comment: req.body.comment,
					medias: req.body.medias,
					active: true,
					coordinate
				},
					{ meta: { user: req.user } });
				return true;
			});




			fastify.post('/rest/registeredUsers/groupsRemove', async (req) => {
				await this.broker.call('registeredUsers.groupsRemove',
					{ group_id: req.body.id },
					{ meta: { user: req.user } });
				return true;
			});

			fastify.get('/api/topics/find',
				async (req) =>
					this.broker.call('topics.find',
						{
							query: {
								active: true,
							},
							sort: 'order'
						},
						{ meta: { user: req.user } })
			);

			fastify.get('/api/topicquestions/find',
				async (req) =>
					this.broker.call('topicquestions.find',
						{
							query: {
								topic: req.query.topic,
							}
							, populate: ['createdBy']
						},
						{ meta: { user: req.user } })
			);

			fastify.post('/api/topicquestions/create', async (req) => {
				return await this.broker.call('topicquestions.create', { ...req.body }, { meta: { user: req.user } })
			})

			fastify.post('/api/topicquestions/like', async (req) => {
				return await this.broker.call('topicquestions.like', {
					...req.body
				}, { meta: { user: req.user } })
			})

			fastify.post('/api/topicquestions/unlike', async (req) => {
				return await this.broker.call('topicquestions.unlike', {
					...req.body
				}, { meta: { user: req.user } })
			})

			fastify.delete('/api/topicquestions/:id', async (req) => {
				await this.broker.call('topicquestions.remove', { id: req.params.id }, { meta: { user: req.user } });
				return true;
			});

			//comments
			fastify.get('/api/comments',
				async (req) =>
					this.broker.call('comments.byUser',
						{
							limit: req.query.limit || req.query.increment,
							skip: req.query.skip || req.query.limit_min,
							postId: req.query.postId
						},
						{ meta: { user: req.user } })
			);

			fastify.get('/api/comments/:id/likes', async (req) => this.broker.call('comments.likes', { post_id: req.params.id }, { meta: { user: req.user } }));
			fastify.post('/api/comments/:id/likes', async (req) => {
				await this.broker.call('comments.like', {
					post_id: req.params.id,
					...req.body
				},
					{ meta: { user: req.user } });
				return true;
			});
			fastify.delete('/api/comments/:id', async (req) => {
				await this.broker.call('comments.remove', { id: req.params.id }, { meta: { user: req.user } });
				return true;
			});
			fastify.post('/api/comments', async (req) => {
				await this.broker.call('comments.create', {
					comment: req.body.comment,
					postId: req.body.postId,
					medias: req.body.medias || [],
					active: true
				},
					{ meta: { user: req.user } });
				return true;
			});

			fastify.get('/api/news', async (req) => {
				let now = new Date();
				let result = await this.broker.call('news.find', {
					sort: '-date',
					query: {
						active: true,
						date: { $lte: now },
						endDate: { $gte: now },
						$or: [{ registeredUsers: req.user._id }, { registeredUsers: { $size: 0 } }]
					}
				});
				return result;
			});
			fastify.get('/api/news/:id', async (req) => await this.broker.call('news.get', { id: req.params.id }));

			fastify.get('/api/events', async (req) => this.broker.call('events.groupByTime', { search: req.query.search }, { meta: { user: req.user } }));
			fastify.get('/api/events/:id', async (req) => this.broker.call('events.get', { id: req.params.id }, { meta: { user: req.user } }));
			fastify.delete('/api/events/:id/joins', async (req) =>
				this.broker.call('events.leave', { event_id: req.params.id }, { meta: { user: req.user } }));
			// fastify.delete('/api/events/:id/joins/:user_id', async (req) =>
			// 	this.broker.call('events.leave', { event_id: req.params.id }, { meta: { user: { _id: req.params.user_id } } } ));
			fastify.post('/api/events/:id/joins', async (req) =>
				this.broker.call('events.join', { event_id: req.params.id }, { meta: { user: req.user } }));


			fastify.get('/api/campaigns', async (req) => this.broker.call('campaings.byCategory', { ...req.query }));
			fastify.get('/api/campaigns/:id', async (req) => this.broker.call('campaings.get', { id: req.params.id }));
			fastify.get('/api/campaings', async (req) => this.broker.call('campaings.byCategory', { ...req.query }));
			fastify.get('/api/campaings/:id', async (req) => this.broker.call('campaings.get', { id: req.params.id }));
			fastify.get('/api/coordinates', async (req) => {
				let search = req.query.search;

				if (search && search.length > 0) {
					let ids = await this.broker.call('search.coordinateSearch', { text: search });
					let result = await this.broker.call('coordinates.find', {
						query: {
							_id: { $in: ids.map(x => x._id) },
							type: { $in: req.query.types ? req.query.types.split(',') : [] }
						}
					});

					return result.slice(0, 20);
				}
				else {
					return this.broker.call('coordinates.find', { query: { type: { $in: req.query.types ? req.query.types.split(',') : [] } } });
				}
			});

			fastify.get('/api/blood-donations', async (req) => this.broker.call('bloodDonations.actives', { cities: req.query.cities }));
			fastify.post('/api/blood-donations', async (req) => {
				await this.broker.call('bloodDonations.create', { ...req.body }, { meta: { user: req.user } });
				return true;
			});

			fastify.get('/api/blood-needs', async () =>
				this.broker.call('bloodNeeds.find', {
					query: {
						archive: { $ne: true }
					},
					sort: 'startDate'
				}));
			fastify.post('/api/blood-needs', async (req) => {
				await this.broker.call('bloodNeeds.create', { ...req.body }, { meta: { user: req.user } });
				return true;
			});
			fastify.delete('/api/blood-needs/:id', async (req) => {
				await this.broker.call('bloodNeeds.remove', { id: req.params.id });
				return true;
			});

			fastify.get('/api/notifications_count', async (req) => this.broker.call('notifications.count', { query: { userId: req.user._id, isRead: false } }));

			fastify.get('/api/notifications', async (req) => await this.broker.call('notifications.find', {
				sort: '-date',
				query: {
					userId: req.user._id
					// $or: [
					// 	{isRead: false},
					// 	{isRead: true, date: {$gte : dayjs().add(-3, 'day') }}
					// ]
				}
			}));

			fastify.post('/api/read_notifications/:id', async (req) => {
				await this.broker.call('notifications.update', {
					_id: req.params.id,
					isRead: true
				});
				return true;
			});

			fastify.get('/api/surveys', async (req) => await this.broker.call('surveys.byUser', { user: req.user }));

			fastify.get('/api/surveys/:id', async (req) => await this.broker.call('surveys.byUserId', { id: req.params.id, user: req.user }));

			fastify.post('/api/surveys/:id', async (req) => {
				if (req.body.questions == null) {
					let resultMessage = await this.broker.call('resultMessage.get', { id: '0021' })
					throw new Error(resultMessage.message)
					//throw Error('questions null olamaz');
				}
				await this.broker.call('surveyAnswers.create', {
					survey_id: req.params.id,
					user_id: req.user._id,
					completed: true,
					questions: req.body.questions
				});
				return true;
			});


			fastify.get('/api/surveyAnswers/chartData', async (req) => {
				let params = qs.parse(req.query)
				return await this.broker.call(`surveyAnswers.chartData`, params, { meta: { user: req.user } })

			});

			fastify.get('/rest/survey/countAnswers/:id', async (req) => {

				return await this.broker.call(`surveys.countAnswers`, req.params, { meta: { user: req.user } })
			});
			// fastify.post("/surveys/:survey_id/questions/:question_id", async (req, res) =>
			// 	broker.call("surveys.answerQuestion", {...req.params, ...req.body}, {meta: {user: req.user}} ))


			fastify.get('/api/surveysWithMedia', async (req) => await this.broker.call('surveysWithMedia.byUser', { user: req.user }));
			fastify.post('/api/surveysWithMediaCorrectAnswer', async (req) => await this.broker.call('surveysWithMedia.correctAnswer', { ...req.body }));
			fastify.get('/api/surveysWithMedia/:id', async (req) => await this.broker.call('surveysWithMedia.byUserId', { id: req.params.id, user: req.user }));

			fastify.post('/api/surveysWithMedia/:id', async (req) => {
				if (req.body.questions == null) {
					let resultMessage = await this.broker.call('resultMessage.get', { id: '0021' })
					throw new Error(resultMessage.message)
				}
				await this.broker.call('surveysWithMediaAnswers.create', {
					survey_id: req.params.id,
					user_id: req.user._id,
					completed: req.body.completed ? true : false,
					questions: req.body.questions
				});
				return true;
			});
			fastify.get('/api/surveyWithMediaAnswers/chartData', async (req) => {
				let params = qs.parse(req.query)
				return await this.broker.call(`surveyWithMediaAnswers.chartData`, params, { meta: { user: req.user } })

			});

			fastify.get('/rest/surveyWithMedia/countAnswers/:id', async (req) => {

				return await this.broker.call(`surveysWithMedia.countAnswers`, req.params, { meta: { user: req.user } })
			});
			fastify.get('/api/cities', async () => {
				return this.broker.call('cities.find', { query: { _id: { $ne: '0' } }, sort: 'order,name' });
			});

			fastify.get('/api/supports', async (req) => {
				if (req.user.role == 'admin') {
					return await this.broker.call('supports.forAdmin');
				} else {
					let resultMessage = await this.broker.call('resultMessage.get', { id: '0013' })
					throw new Error(resultMessage.message)
					throw new Error('Yetkisiz işlem');
				}
			});

			fastify.get('/api/tickets', async (req) => {
				return await this.broker.call('tickets.find', {
					fields: ['_id', 'title', 'state', 'createdAt'],
					sort: '-createdAy',
					query: { userId: req.user._id }
				});
			});

			fastify.get('/api/ticket_messages/:id', async (req) => {
				let result = await this.broker.call('tickets.find', { query: { _id: req.params.id, userId: req.user._id } });
				if (result.length > 0)
					return result[0];
				else {
					let resultMessage = await this.broker.call('resultMessage.get', { id: '0022' })
					throw new Error(resultMessage.message)
					//throw new Error('ticket bulunamadı');
				}
			});

			fastify.post('/api/tickets', async (req) => {
				return await this.broker.call('tickets.create', { ...req.body }, { meta: { user: req.user } });
			});

			fastify.post('/api/ticket_messages/:id', async (req) => {
				return await this.broker.call('tickets.message', { ...req.body, ticketId: req.params.id }, { meta: { user: req.user } });
			});

			fastify.post('/api/supports/message', async (req) => {
				if (req.user.role == 'admin') {
					return await this.broker.call('supports', { query: { active: true } });
				} else {
					let resultMessage = await this.broker.call('resultMessage.get', { id: '0013' })
					throw new Error(resultMessage.message)
					//	throw new Error('Yetkisiz işlem');
				}
			});

			fastify.post('/api/meetings/create', async (req, res) => {
				return await this.broker.call('meetings.create', { ...req.body }, { meta: { user: req.user } })
			})

			fastify.post('/api/meetings/delete', async (req, res) => {
				return await this.broker.call('meetings.remove', { ...req.body }, { meta: { user: req.user } })
			})

			fastify.get('/api/meetings/list', async (req, res) => {
				return await this.broker.call('meetings.find', {
					query: { endAt: { $gte: dayjs().toDate() } },
					fields: ['_id', 'topic', 'startAt', 'endAt', 'createdBy', "isPrivate"],
					sort: '-startAt'
				}, { meta: { user: req.user } })
			})

			fastify.post('/api/meetings/getKey', async (req, res) => {
				return await this.broker.call('meetings.getKey', { ...req.body })
			})

			fastify.get('/rest/reports/:id', async (req) => {
				let { service, id } = req.params
				//let params = qs.parse(req.query)
				return await this.broker.call(`reports.${id}`, req.query, { meta: { user: req.user } })
			})
			fastify.get("/api/bill", async (req) =>
        		this.broker.call("cron.bill", { ...req.body })
      		);
      		fastify.get("/api/deletebill", async (req) =>
				this.broker.call("cron.deletebill", { ...req.body })
			);
			fastify.get("/api/deletingOldDataBill", async (req) =>
				this.broker.call("cron.deletingOldDataBill", { ...req.body })
			);
			fastify.get("/api/subscribers", async (req) =>
				this.broker.call("cron.subscriber", { ...req.body })
			);
			fastify.get("/api/deletesubscriber", async (req) =>
				this.broker.call("cron.deletesubscriber", { ...req.body })
			);
			fastify.get("/api/location", async (req) =>
				this.broker.call("cron.location", { ...req.body })
			);

			fastify.get("/api/installment", async (req) =>
				this.broker.call("installment.getmove", { ...req.body })
			);
					// fastify.post('/rest/registeredUsers/leaveUsers', async(req) => {
			// 	let {service} = req.params
			// 	return await this.broker.call(`registeredUsers.leaveUsers`, req.body, {meta: {user: req.user}})
			// })

			// fastify.post('/rest/registeredUsers/confirm', async(req) => {
			// 	return await this.broker.call(`registeredUsers.confirm`, req.body, {meta: {user: req.user}})
			// })

			// fastify.post('/rest/registeredUsers/reject', async(req) => {
			// 	return await this.broker.call(`registeredUsers.reject`, req.body, {meta: {user: req.user}})
			// })

			//limit, pageSize, sort || rows
			fastify.get('/api/informations/list', async (req, res) => {
				return await this.broker.call('informations.list', {
					page: req.query.page || 0,
					pageSize: req.query.pageSize || 20,
					sort: '-createdAt',
					query: {
						active: true
					}
				}, { meta: { user: req.user } })
			})

			fastify.get('/api/informations/find', async (req, res) => {
				return await this.broker.call('informations.find', {
					query: {
						...req.query,
						active: true
					}
				}, { meta: { user: req.user } })
			})

			fastify.post('/api/informations/create', async (req, res) => {
				return await this.broker.call('informations.create', { ...req.body }, { meta: { user: req.user } })
			})

			fastify.post('/api/informations/update/:id', async (req, res) => {
				return await this.broker.call('informations.update', { _id: req.params.id, ...req.body }, { meta: { user: req.user } })
			})

			fastify.post('/api/informations/delete/:id', async (req, res) => {
				return await this.broker.call('informations.remove', { id: req.params.id }, { meta: { user: req.user } })
			})

			fastify.post('/rest/notifications/itemsRemove', async (req) => {
				await this.broker.call('notifications.itemsRemove',
					{ item_id: req.body.id },
					{ meta: { user: req.user } });
				return true;
			});

			fastify.post('/rest/shakeWinItem/itemsRemove', async (req) => {
				await this.broker.call('shakeWinItem.itemsRemove',
					{ item_id: req.body.id },
					{ meta: { user: req.user } });
				return true;
			});

			fastify.post('/rest/speakers/sessionRemove', async (req) => {
				return await this.broker.call('speakers.sessionRemove',

					{ item_id: req.body.id},
					{ meta: { user: req.user } });

			});

			fastify.post('/rest/sessions/speakerRemove', async (req) => {
				return await this.broker.call('sessions.speakerRemove',

					{ item_id: req.body.id},
					{ meta: { user: req.user } });

			});

			fastify.post('/rest/notifications/pushNotificationRemove', async (req) => {
				await this.broker.call('notifications.pushNotificationRemove',
					{ pushNotificationId: req.body.id },
					{ meta: { user: req.user } });
				return true;
			});

			fastify.get('/api/login/cron', async (req, res) => {
				return await this.broker.call('registeredUsers.logincron', { id: req.params.id }, { meta: { user: req.user } });
			})

			fastify.get('/rest/users/:id', async (req, res) => {
				return await this.broker.call('registeredUsers.get', { id: req.params.id }, { meta: { user: req.user } });
			})

			fastify.delete('/rest/registeredUsers/:id', async (req) => {

				return await this.broker.call(`registeredUsers.deleteduser`, { _id: req.params.id, ...req.body }, { meta: { user: req.user } });
			})
			fastify.put('/rest/users/:id', async (req, res) => {
				return await this.broker.call('registeredUsers.update', { _id: req.params.id, ...req.body }, { meta: { user: req.user } });

			})
			fastify.get('/api/competitionAnswers/chartData', async (req) => {
				let params = qs.parse(req.query)
				return await this.broker.call(`competitionAnswers.chartData`, params, { meta: { user: req.user } })
			});
			fastify.get('/rest/competitions/countAnswers/:id', async (req) => {

				return await this.broker.call(`competitions.countAnswers`, req.params, { meta: { user: req.user } })
			});
			fastify.get('/rest/surveysWithMedia/countAnswers/:id', async (req) => {

				return await this.broker.call(`surveysWithMedia.countAnswers`, req.params, { meta: { user: req.user } })
			});

			fastify.get('/rest/:service', async (req) => {

				let { service } = req.params
				let params = qs.parse(req.query)
				return await this.broker.call(`${service}.list`, params, { meta: { user: req.user } })
			})

			fastify.get('/rest/:service/:id', async (req) => {
				let { service, id } = req.params
				//let params = qs.parse(req.query)
				return await this.broker.call(`${service}.get`, { id: id }, { meta: { user: req.user } })
			})

			fastify.post('/rest/:service', async (req) => {
				let { service, action } = req.params
				return await this.broker.call(`${service}.create`, req.body, { meta: { user: req.user } })
			})

			fastify.post('/rest/:service/:action', async (req) => {
				let { service, action } = req.params
				return await this.broker.call(`${service}.${action}`, req.body, { meta: { user: req.user } })
			})

			fastify.put('/rest/:service/:id', async (req) => {
				let { service, action, id } = req.params
				return await this.broker.call(`${service}.update`,
					{ ...req.body },
					{ meta: { user: req.user } })
			})


			fastify.delete('/rest/:service/:id', async (req) => {
				let { service, id } = req.params
				return await this.broker.call(`${service}.remove`,{ id: id })
			})

			fastify.get('/rest/surveyWithMediaAnswers/getNameAnswer', async (req) => {
				let params = qs.parse(req.query)
				return await this.broker.call(`surveyWithMediaAnswers.getNameAnswer`, params, { meta: { user: req.user } })
			});
			fastify.get('/rest/competitionAnswers/getNameAnswer', async (req) => {
				let params = qs.parse(req.query)
				return await this.broker.call(`competitionAnswers.getNameAnswer`, params, { meta: { user: req.user } })
			});
			fastify.get('/rest/shakeWinWinners/getNameWinner', async (req) => {
				let params = qs.parse(req.query)
				return await this.broker.call(`shakeWin.getNameWinner`, params, { meta: { user: req.user } })

			});
			fastify.get('/rest/shakeWinWinners/getWinnerItems', async (req) => {
				let params = qs.parse(req.query)
				return await this.broker.call(`shakeWin.getWinnerItems`, params, { meta: { user: req.user } })

			});

			fastify.post('/api/upload', async (req) => {
				let files = [];

				if (req.body.files != null && req.body.files.length > 0) {
					files = req.body.files;
				}
				if (req.body.files_0 != null) {

					for (var i = 0; ; i++) {
						if (req.body[`files_${i}`] != null) {
							req.body[`files_${i}`][0].resize = req.body[`resize`]
							files.push(req.body[`files_${i}`][0]);
						}
						else {
							break;
						}
					}
				}


				return await this.broker.call('storage.save', { files });
			});

			//console.log(__dirname)

			fastify.register(require('fastify-static'), {
				root: path.join(__dirname, '../public/files'),
				prefix: '/files', // optional: default '/'
			})

			// fastify.register(require('fastify-static'), {
			// 	root: path.join(__dirname, '../public/images'),
			// 	prefix: '/images', // optional: default '/'
			// })

		} catch (err) {
			console.log(err);
		}
	},

	started() {
		this.fastify.listen(this.settings.port, '0.0.0.0').then(
			() => {
			},
			(err) => console.error(err));
	},

	async stopped() {
		return await this.fastify.close();
	}

};
