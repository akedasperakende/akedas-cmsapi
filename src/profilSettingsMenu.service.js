
'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');

module.exports = {
	name: 'profilSettingsMenu',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'profilSettingsMenu',

	settings: {
		entityValidator: {
			$$strict: true,
            _id: {type: 'string'},
            lang:'string',
            type: 'string',
            name: 'string',
            iconUrl: 'string',
            active: 'boolean',
            order: 'number'
		}
    },
    hooks: {
		before: {
			list: [(ctx) => {

				ctx.params.query = {}

				if(ctx.params.lang)
                    ctx.params.query.lang  = ctx.params.lang
                    
				ctx.params.pageSize = 100;
			}]

        }
    }
};