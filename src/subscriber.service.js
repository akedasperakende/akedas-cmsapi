'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
let nanoid = require("nanoid");
const dayjs = require("dayjs");
var sql = require("mssql");
var config = {
   user: process.env.USER,
 
   password: process.env.PASSWORD,
 
   server: process.env.SERVER,
 
   database: process.env.DATABASE,
 
   dialect: process.env.DIALECT,
   connectionTimeout: 600000,
   requestTimeout: 600000,
   pool: {
     idleTimeoutMillis: 600000,
     max: 100,
   },
   dialectOptions: {
     instanceName: "SQLEXPRESS",
   },
 
   options: {
     trustedConnection: true,
     encrypt: true,
     enableArithAbort: true,
     trustServerCertificate: true,
   },
 };
 
module.exports = {
   name: "subscriber",
   mixins: [DbService],
   adapter: new MongoDBAdapter(
      process.env.MONGO_URL,
      {useNewUrlParser: true, useUnifiedTopology: true},
      process.env.DB
   ),
   collection: "abone",
   settings: {
      entityValidator: {
         $$strict: true,
         _id: {type: 'string'},
         ABONE_TURU_STNT: {type: 'string'},
         ETSO_KOD: {type: 'string'},
         GERILIM_GRUBU: {type: 'string'},
         SOZLESME_GUCU: {type: 'string'},
         ABONE_GRUBU: {type: 'string'},
         ZAMAN_TARIFESI: {type: 'string'},
         KURULU_GUC: {type: 'number'},
         NAKIT_GB: {type: 'number'},
         AD: {type: 'string'},
         TESISAT: {type: 'string'},
         KESIK_DURUM: {type: 'string'},
         SOYAD: {type: 'string'},
         DURUM: {type: 'string'},
         ST_ANLASMA_TURU: {type: 'string'},
         ADRES: {type: 'string'},
         MAHALLE: {type: 'string'},
         ILCE: {type: 'string'},
         MAIL: {type: 'string'},
         BOLGE: {type: 'string'},
         VERGI_NO: {type: 'string'},
         TEMINAT_MEKTUBU_GB: {type: 'string'},
         SOZLESME_NO: {type: 'string'},
         ABONE_SINIFI: {type: 'string'},
         IL: {type: 'string'},
         SOZLESME_HESAP_NO: {type: 'string'},
         MUHATAP_NO: {type: 'string'},
         TEL_NO: {type: 'string'},
         TC_NUMARASI: {type: 'string'},
         TEKIL_KOD: {type: 'string'},
         KULLANIM_TIPI: {type: 'string'},
         YUKLEME_TARIH: {type: 'date'},
         ABONELIK_TARIH: {type: 'date'},
         ST_TAAHHUT_TARIHI: {type: 'date'},
         ABONELIK_IPTAL_TARIH: {type: 'date'},
      }
   },

   actions: {
      
      async archive() {
         this.adapter.updateMany({
            endDate: {$lte: new Date()}
         }, {
            $set: {
               archive: true
            }
         });
      },
      async changeSubscriber(ctx) {

         let pool = await sql.connect(config);
         const request = pool.request();
         
         let today="abone_"+ctx.params.today
         let yesterday="abone_"+ctx.params.yesterday
         const partial_result = await request.query(
          " select * from "+today+" except  select * from "+yesterday);
   
        let   record = await partial_result.recordset.map((r) => {
         (r._id = nanoid(25)),
         (r.TESISAT =r.TESISAT== null ? null:  String(r.TESISAT)),
         (r.KURULU_GUC =r.KURULU_GUC== null ? null:  Number(r.KURULU_GUC)),
         (r.GERILIM_GRUBU=r.GERILIM_GRUBU == null ? null: String(r.GERILIM_GRUBU)),
         (r.ETSO_KOD =r.ETSO_KOD == null ? null: String(r.ETSO_KOD)),
         (r.ABONE_TURU_STNT =r.ABONE_TURU_STNT == null ? null: String(r.ABONE_TURU_STNT)),
         (r.TEKIL_KOD =r.TEKIL_KOD == null ? null: String(r.TEKIL_KOD)),
         (r.TEL_NO = r.TEL_NO== null ? null: String(r.TEL_NO)),
         (r.MUHATAP_NO =r.MUHATAP_NO == null ? null: String(r.MUHATAP_NO)),
         (r.TC_NUMARASI =r.TC_NUMARASI == null ? null: String(r.TC_NUMARASI)),
         (r.SOZLESME_HESAP_NO =r.SOZLESME_HESAP_NO == null ? null: String(r.SOZLESME_HESAP_NO)),
         (r.SOZLESME_NO =r.SOZLESME_NO == null ? null: String(r.SOZLESME_NO)),
         (r.VERGI_NO =r.VERGI_NO  == null ? null: String(r.VERGI_NO)),
         (r.ILCE =r.ILCE == null ? null: String(r.ILCE)),
         (r.MAHALLE =r.MAHALLE == null ? null: String(r.MAHALLE)),
         (r.ADRES =r.ADRES == null ? null: String(r.ADRES)),
         (r.DURUM =r.DURUM == null ? null: String(r.DURUM)),
         (r.ZAMAN_TARIFESI =r.ZAMAN_TARIFESI == null ? null: String(r.ZAMAN_TARIFESI)),
         (r.ABONE_GRUBU =r.ABONE_GRUBU == null ? null: String(r.ABONE_GRUBU)),
         (r.AD =r.AD == null ? null: String(r.AD)),
         (r.SOYAD = r.SOYAD== null ? null: String(r.SOYAD));
         (r.ABONE_SINIFI =r.ABONE_SINIFI == null ? null: String(r.ABONE_SINIFI)),
         (r.ST_TAAHHUT_TARIHI =r.ST_TAAHHUT_TARIHI == null ? null: dayjs(r.ST_TAAHHUT_TARIHI).toDate()),
         (r.ST_ANLASMA_TURU =r.ST_ANLASMA_TURU == null ? null: String(r.ST_ANLASMA_TURU)),
         (r.KESIK_DURUM = r.KESIK_DURUM == null ? null: String(r.KESIK_DURUM)),
         (r.NAKIT_GB =r.NAKIT_GB == null ? null: Number(r.NAKIT_GB)),
         (r.SOZLESME_GUCU = r.SOZLESME_GUCU== null ? null: String(r.SOZLESME_GUCU)),
         (r.TEMINAT_MEKTUBU_GB =r.TEMINAT_MEKTUBU_GB == null ? null: Number(r.TEMINAT_MEKTUBU_GB)),
         (r.IL =r.IL == null ? null: String(r.IL)),
         (r.BOLGE =r.BOLGE == null ? null: String(r.BOLGE)),
         (r.MAIL =r.MAIL == null ? null: String(r.MAIL)),
         (r.ABONELIK_TARIH =r.ABONELIK_TARIH == null ? null: dayjs(r.ABONELIK_TARIH).toDate()),
         (r.ABONELIK_IPTAL_TARIH =r.ABONELIK_IPTAL_TARIH == null ? null: dayjs(r.ABONELIK_IPTAL_TARIH).toDate()),
         (r.KULLANIM_TIPI =r.KULLANIM_TIPI == null ? null: String(r.KULLANIM_TIPI)),
         (r.IBAN =r.IBAN == null ? null: String(r.IBAN)),
         (r.IBAN_KISI =r.IBAN_KISI == null ? null: String(r.IBAN_KISI));
         return r;
       });
        let addData= await this.adapter.insertMany(record);
        return addData.length
        
       },
      async deleteSubscriber(ctx) {

         let pool = await sql.connect(config);
         const request = pool.request();
         let today="abone_"+ctx.params.today
         let yesterday="abone_"+ctx.params.yesterday
         const partial_result = await request.query(
          " select * from "+yesterday+" except  select * from "+today);
   
         let rem = await this.adapter.removeMany({
           SOZLESME_HESAP_NO: {
              $in: partial_result.recordset.map((x) => x.SOZLESME_HESAP_NO)
         }
         });
   
         return rem
       },
      async getByField(ctx){
         return  await this.broker.call("subscriber.find", {
            query: {
               [ctx.params.field]: ctx.params.value
            },
            fields: ['_id', 'AD', "SOYAD", "MAIL", "TEL_NO", "SOZLESME_HESAP_NO", "VERGI_NO"]
         });
      }
   },
};
