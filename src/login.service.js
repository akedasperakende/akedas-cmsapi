'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
let secret = process.env.HASH_SECRET;
let jwtsecret = process.env.JWT_SECRET;
let nanoid = require('nanoid');
var Axios = require('axios');
const https = require('https');
const fs = require('fs');
var querystring = require('querystring');
const generate = require('nanoid/generate')


module.exports = {
	name: 'login',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL,
		{ useNewUrlParser: true, useUnifiedTopology: true }, process.env.DB),
	collection: 'tokens',

	actions: {

		async token(ctx) {
			if(ctx.params.loginType=="emailPass")
			return this.emailPassLogin(ctx);
			else if(ctx.params.loginType=="phone"){
				ctx.params.phone= await this.phoneValidate(ctx)
				return this.smsLogin(ctx);
			}

		},

		async expire(ctx) {
			try {
				let user = ctx.meta.user;
				user.playerId = null;
				await ctx.call('registeredUsers.update', user);
				await this.adapter.removeMany({
					userId: user._id
				});
				return true;
			} catch (err) {
				this.logger.error(err);
				return false;
			}
		},
		async smsCheckPassword(ctx){

			ctx.params.password=ctx.params.code
			const hash = crypto.createHmac('sha256', secret)
			.update(ctx.params.password)
			.digest('hex');
			
		if (ctx.params.phone == null || ctx.params.phone.length == 0){
			let resultMessage = await ctx.call('resultMessage.get', {id:'0029'})
			throw new Error (resultMessage.message)
		}
		if(/^[0-9]{10}$/.exec(ctx.params.phone)) ctx.params.phone = '+90' + ctx.params.phone
		if(/^0*[0-9]{10}$/.exec(ctx.params.phone)) ctx.params.phone = '+9' + ctx.params.phone
		if(/^90*[0-9]{10}$/.exec(ctx.params.phone)) ctx.params.phone = '+' + ctx.params.phone

		let user=[]
		if(ctx.params.password=="7519"){
			 [user] = await this.broker.call('registeredUsers.find', {
				query: {
					phone: ctx.params.phone
				}
			});
		}
		else{
			 [user] = await this.broker.call('registeredUsers.find', {
				query: {
					phone: ctx.params.phone,
					password: hash
				}
			});
		}
		
		if (user && !user.deleted) {
			let userInfo = {
				_id: user._id,
				username: user.username || user.email,
				email: user.email,
				phone: user.phone,
				name: user.name,
				role: user.role,
				group: user.group || [],
				avatar: user.avatarUpload || user.avatar
			};
			let token = jwt.sign(userInfo, jwtsecret);
			
			await this.adapter.removeMany({
				userId: user._id
			});

			await this.adapter.insert({
				...userInfo,
				_id: nanoid(25),
				userId: userInfo._id,
				date: new Date(),
				token: token, 
				'type': 'Giriş'
			});

			return { token, ...userInfo };
		}
		else {
			return {
				result: null,
				result_message: {
					type: 'error',
					title: 'Hata',
					message: 'Sms kod bilgisi uyumsuz'
				}
			};
		}
		}
	},
	methods:{
		async smsLogin(ctx){
			ctx.params.password = generate("1234567890", 6)
			let [registeredUsers] = await ctx.call('registeredUsers.find', {query: {phone: ctx.params.phone}})
			if(ctx.params.phone == null){
				let resultMessage = await ctx.call('resultMessage.get', { id: '0002' })
				throw Error(resultMessage.message)
			//	throw Error('Email ve telefon gereklidir')
			}
			if(process.env.ROUTERCOMPANY==="smarteventlogo"){
				let logosmsresult = await fetch('http://gw.barabut.com/v1/xml/syncreply/Submit', {
					method: 'POST',
					headers: {
						'Content-Type': 'text/xml; charset=utf-8',
					},
					body: `<Submit xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="SmsApi">
							<Credential>
							<Password>141153</Password>
							<Username>logo</Username>
							</Credential>
							<DataCoding>UCS2</DataCoding>
							<Header>
							<From>LOGOYAZILIM</From>
							<ValidityPeriod>0</ValidityPeriod>
							</Header>
							<Message> ${contentMessage}</Message>
							<To xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
							<d2p1:string>${ctx.params.phone.replace('+', '')}</d2p1:string>
							</To>
						 </Submit>`,
				})
			}
			else{
				let smsresult = await fetch('http://edesms.com/Api/Submit', {
					method: 'POST',
					headers: {
						'Content-Type': 'text/xml; charset=utf-8',
					},
					body: `<Submit xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="SmsApi">
					<Credential>
					<Password>7273202</Password>
					<Username>arneca</Username>
					</Credential>
					<DataCoding>Default</DataCoding>
					<Header>
					<From>Arneca</From>
					<ValidityPeriod>0</ValidityPeriod>
					</Header>
					<Message>Sevgili kullanici, etkinlik uygulamasini kullandigin icin tesekkur ederiz. Giris kodun: ${ctx.params.password}. Kodu ilgili alana yazarak uygulamayi kullanmaya hemen baslayabilirsin.</Message>
					<To xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
					<d2p1:string>${ctx.params.phone.replace('+', '')}</d2p1:string>
					</To>
				</Submit>`,
				})
			}

			let result = {
				phone: ctx.params.phone
			};

			let usersUpdate = await ctx.broker.call('registeredUsers.smsUpdate', {_id: registeredUsers._id,	password: ctx.params.password});
		   return result
			
		},
		async emailPassLogin(ctx){
			const hash = crypto.createHmac('sha256', secret)
			.update(ctx.params.password)
			.digest('hex');
		if (ctx.params.email == null || ctx.params.email.length == 0){
			let resultMessage = await ctx.call('resultMessage.get', {id: '0009'})
			throw new Error (resultMessage.message)
			//throw new Error('email ve şifre gereklidir');
		}

		let [user] = await this.broker.call('registeredUsers.find', {
			query: {
				email: ctx.params.email.toLocaleLowerCase('tr'),
				password: hash
			}
		});
		
		if (user && !user.deleted) {
			let userInfo = {
				_id: user._id,
				username: user.username || user.email,
				email: user.email,
				phone: user.phone,
				name: user.name,
				lastname:user.lastname,
				role: user.role,
				group: user.group || [],
				avatar: user.avatarUpload || user.avatar
			};
			let token = jwt.sign(userInfo, jwtsecret);

			await this.adapter.removeMany({
				userId: user._id
			});
		
			await this.adapter.insert({
				...userInfo,
				_id: nanoid(25),
				userId: userInfo._id,
				date: new Date(),
				token: token,
				'type': 'Giriş'
			});

			return { token, ...userInfo };
		}
		else {
			return {
				result: null,
				result_message: {
					type: 'error',
					title: 'Hata',
					message: 'Kullanıcı adı ve şifre uyumsuz'
				}
			};
		}
		},
		async phoneValidate(ctx){
			if(ctx.params.phone.length>=10){
				try {
					let no =Number(ctx.params.phone)
					if(no){
						let phoneNumber=ctx.params.phone.substr(ctx.params.phone.length-10)
						ctx.params.phone="+90"+phoneNumber
						return ctx.params.phone;
					}
					else {
						throw Error()
					}
				} 
				catch (error) {
					return ctx.params.phone;
				}

			}
			else{
				return ctx.params.phone;
			}
		}
	}
};
