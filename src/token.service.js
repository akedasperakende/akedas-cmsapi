'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');

module.exports = {
	name: 'tokens',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'tokens',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			user_id: {type: 'string'},
			name:{type: 'string'},
			token: 'string',
			username: 'string'
		}
	},
	actions: {
		async userIdRemove (ctx) {
			let userId = ctx.params.userId;
			await this.adapter.removeMany({ userId: userId });
		},
	}
};