'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');

const dayjs = require('dayjs');
const nanoid = require('nanoid');

module.exports = {
	name: 'surveys',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'surveys',


	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			lang:{type: 'string', optional: true},
			name:{type: 'string'},
			active: 'boolean',
			created: 'date',
			// endDate: 'date',
			// startDate: 'date',
			registeredUsers: [{type :'array', items: 'string', optional: true}],
			groups:  [{type :'array', items: 'string', optional: true}],		
			sendNotification: {type: 'boolean', optional: true},
			createdAt: 'date',
			createdBy: 'string',
			updatedAt: 'date',
			updatedBy: 'string',
			usersFile: {'type': 'object', optional: true, props: {
				type: {type:'string'},
				thumb: {type:'string'},
				url: {type:'string'},
				_id: {type:'string'},
				mimeType: {type:'string'}
			}},
			questions: {
				'type': 'array',
				items: {
					type: 'object',
					props: {
						type: {type: 'string', enum: ['S','F','M']},
						name: 'string',
						is_answered: 'boolean',
						other: {type: 'string', optional: true},
						choices: {
							type: 'array',
							items: {
								type: 'object',
								props: {
									name: 'string',
									is_other: 'boolean',
									other: 'string',
									is_selected: 'boolean'
								}
							}
						}
					}
				}
			}
		}
	},

	actions: {
		// async archive() {
		// 	this.adapter.updateMany({
				
		// 	}, {
		// 		$set : {
		// 			archive: true
		// 		}
		// 	});
		// },
		async byUser (ctx) {
			let user = ctx.params.user;

			let now = new Date();
			let surveys = await this._find(ctx, {
				query: {
					active: true,
					// startDate: {$lte: now},
					// endDate: {$gte: now},
					$or: [{registeredUsers: user._id}, {registeredUsers: {$size: 0}}]
				},
				fields: ['_id', 'name']
			});

			let filleds = await this.broker.call('surveyAnswers.find', 
				{query: {
					user_id : user._id,
					survey_id: {$in: surveys.map(x => x._id)} 
				}});

			surveys = surveys.map(survey => {
				let filled = filleds.find(f => f.survey_id === survey._id);
				if(filled){
					survey.completed = true;
				}

				return survey;

			});

			return surveys;
		},
	

		async byUserId (ctx) {
			let user = ctx.params.user;
			let survey = await this.broker.call('surveys.get', {id: ctx.params.id , fields: ['_id', 'name', 'completed', 'questions'] });
			let filleds = await this.broker.call('surveyAnswers.find', 
				{query: {
					user_id : user._id,
					survey_id: survey._id
				}});

			if(filleds.length > 0){
				survey.completed = true;
				survey.questions = filleds[0].questions;
			}

			return survey;

		},
		async countAnswers(ctx) {
			let answers = await this.broker.call('surveyAnswers.find',
				{
					query: {
						survey_id: ctx.params.id
					}
				});
			return answers.length;

		},

	},

	entityCreated: async (json, ctx) => {

		let language =  json.lang.toLocaleLowerCase('tr')
		let message = await ctx.call('pushNotificationMessage.find',{ query: {code: "12",language: language }})

		if(json.sendNotification){
			let surveyIcon = {
				'_id' : 'survey.png',
				'url' : process.env.CDN + '/modules/survey.png',
				'thumb' :  process.env.CDN + '/modules/survey.png',
				'type' : 'image',
				'mimeType' : 'image/png'
			};
			
			ctx.broker.call('pushNotifications.create', {
				lang:json.lang,
				date: json.created,
				title: message[0].title,
				active: json.active,
				sendNotification: json.sendNotification,
				registeredUsers: json.registeredUsers,
				groups:json.groups,
				content: json.name,
				completed: false,
				icon : surveyIcon,
				media: surveyIcon,
				type: 'survey',
				item_id:json._id
			},
			{ meta: { user: ctx.meta.user }});
		}
	},

	hooks: {
		before: {
			list: async ctx => {
				let search = ctx.params.search
				if (search && search.length > 2) {
					let result = await ctx.broker.call('search.surveySearch', {text: search})
					if (!ctx.params.query)
						ctx.params.query = {}
					ctx.params.query._id = {$in: result.map(r => r._id)}
				}
				delete ctx.params.search;
				let query = ctx.params.query ;
				ctx.params.populate = ['groups'];
				
				if(query && query.archive === 'true'){
					query.archive = query && query.archive === 'true';
				}
				else if(query) query.archive = null;
				
			},
			create: [(ctx) => {
				
				let survey = ctx.params;
				survey._id = nanoid(25);
				survey.created = new Date();
				ctx.params.createdAt = dayjs().toDate();
				ctx.params.createdBy = ctx.meta.user._id;
				survey.updatedAt = dayjs().toDate();
				survey.updatedBy = ctx.meta.user._id;
				// survey.startDate = dayjs(survey.startDate).toDate();
				// survey.endDate = dayjs(survey.endDate).toDate();
				survey.active = survey.active || false;
				survey.registeredUsers = survey.registeredUsers || [];
				survey.questions.forEach(q => {
					q.is_answered = false;
					if(q.choices)
						q.choices.forEach((c,i) => {
							c.other = '';
							c.is_other = i < (q.choices.length - 1) ? false : (c.is_other || false);
							c.is_selected = false;
						});
					else q.choices = [];
				});

			}],

			update: [(ctx) => {
				let survey = ctx.params;
				survey.created = dayjs(ctx.params.created || new Date()).toDate();
				// survey.startDate = dayjs(survey.startDate).toDate();
				// survey.endDate = dayjs(survey.endDate).toDate();
				survey.active = survey.active || false;
				survey.registeredUsers = survey.registeredUsers || [];
				delete ctx.params.createdAt;
				delete ctx.params.createdBy;
				survey.updatedAt = dayjs().toDate();
				survey.updatedBy = ctx.meta.user._id;
				survey.questions.forEach(q => {
					q.is_answered = false;
					if(q.choices)
						q.choices.forEach((c,i) => {
							c.other = '';
							c.is_other = i < (q.choices.length - 1) ? false : (c.is_other || false);
							c.is_selected = false;
						});
					else q.choices = [];
				});

			}]
		},
		after: {
			remove:  [async ctx => {
				await ctx.broker.call('pushNotifications.itemsRemove', { item_id: ctx.params.id });
				await ctx.broker.call('notifications.itemsRemove', { item_id: ctx.params.id });

				return true
			}]
		}

	}
};