'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const dayjs = require('dayjs');
const nanoid = require('nanoid');
const axios = require('axios')
module.exports = {
   name: 'requestAndComplaint',
   mixins: [DbService],
   adapter: new MongoDBAdapter(process.env.MONGO_URL,
      {useNewUrlParser: true, useUnifiedTopology: true}, process.env.DB),
   collection: 'requestAndComplaint',
   settings: {
      entityValidator: {
         $$strict: true,
			_id: { type: 'string' },
			registeredUserId: { type: 'string'},
			SOZLESME_HESAP_NO: { type: 'string'},
			categoryId: { type: 'string'},
			subCategoryId: { type: 'string'},	
			address: { type: 'string' },
         solution: 'boolean',
         transactionNumber:{type:"number"},
			message: { type: 'string'},
         statusMessage:{ type: 'string' },
			createdAt: 'date',
			updatedAt: 'date',

      },
      populates: {
         "registeredUserId": {
            action: "registeredUsers.get",
            params: {
               fields: ["_id","avatar", "tcNo", "vkNo", "telNo", "isCorporate", "email", "name", "lastname"]
            }
         },
         "categoryId": {
            action: "requestAndComplaintCategories.get",
            params: {
               fields: ["_id","name"]
            }
         }
         ,
         "subCategoryId": {
            action: "requestAndComplaintSubCategories.get",
            params: {
               fields: ["_id","name"]
            }
         }
      },

   },
   hooks: {
      before: {
         create: ctx => {
            let x = ctx.params
            x.transactionNumber=Number((Math.random()*100000).toFixed());
            x._id = nanoid(25);
            x.createdAt = dayjs().toDate();
            x.updatedAt = dayjs().toDate();
            x.solution = x.solution || false;
         },
         get: [async (ctx) => {
            ctx.params.populate = ['registeredUserId', 'categoryId', 'subCategoryId']
            let result = await ctx.call("requestAndComplaint.find",{query:{_id:ctx.params.id}})
            console.log(result);
            return result
            
         }],
         find: [(ctx) => {
            ctx.params.populate = ['registeredUserId', 'categoryId', 'subCategoryId']
         }],
			list: async ctx => {
            let search = ctx.params.search
				if (search && search.length > 2) {
					let result = await ctx.broker.call('search.requestAndComplaintSearch', {text: search})
					if (!ctx.params.query)
						ctx.params.query = {}
					ctx.params.query._id = {$in: result.map(r => r._id)}
				}
				delete ctx.params.search;
            ctx.params.populate = ['registeredUserId', 'categoryId', 'subCategoryId']
         },
         update: ctx => {
            let x = ctx.params
            x.transactionNumber=Number((Math.random()*100000).toFixed());
            x.registeredUserId = x.registeredUserId._id
            x.categoryId = x.categoryId._id
            x.subCategoryId = x.subCategoryId._id
            x.updatedAt = dayjs().toDate();
            x.updatedBy = ctx.meta.user._id
         },

      }
   }
};
