'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');

module.exports = {
   name: 'registeredUsers',
   mixins: [DbService],
   adapter: new MongoDBAdapter(process.env.MONGO_URL,
      {useNewUrlParser: true, useUnifiedTopology: true}, process.env.DB),
   collection: 'registeredUsers',

   settings: {
      entityValidator: {
         $$strict: true,
         _id: {type: "string"},
         muhatapNo: {type: "string"},
         tcNo: {type: "string", optional: true},
         vkNo: {type: "string", optional: true},
         name: {type: "string", optional: true},
         lastname: {type: "string", optional: true},
         telNo: {type: "string"},
         password: {type: "string", optional: true},
         code: {type: "string", optional: true},
         role: {type: "string", enum: ["user", "admin"]},
         isCorporate: {type: "boolean", optional: true},
         isKvkk: {type: "boolean", optional: true},
         isLogin: {type: "boolean", optional: true},
         isRegistered: {type: "boolean"},
         deleted: {type: "boolean", optional: true},
         createdAt: "date",
         updatedAt: "date",
         isLoginTime: {type: "date", optional: true},
         playerId: {type: "string", optional: true},
         lang: {type: "string", optional: true},
         avatar: {
            type: "object",
            optional: true,
            props: {
               $$strict: true,
               type: {type: "string"},
               thumb: {type: "string"},
               url: {type: "string"},
               _id: {type: "string"},
               mimeType: {type: "string"},
            },
         },
      },
   },

   actions: {
      async isExpire(ctx) {
         let [result] = await this.broker.call('tokens.find', {userId: ctx.meta.user._id, token: ctx.meta.token})
         if (result)
            return false;
         else
            return true
      },

      async deletedUser(ctx) {
         let id = ctx.params._id
         let d = await this.adapter.updateById(id, {$set: {deleted: true}})
         await ctx.broker.call('tokens.userIdRemove', {userId: id});
         return true

      },

      async birthToday() {
         let today = dayjs().format('M-D');

         let birthdays = await this.adapter.find({query: {show_birthday: true, birthday: today}});
         return birthdays.map(x => ({
            _id: x._id,
            name: x.name, birthday: x.birthday,
            url: x.avatar.url,
            company: x.company,
            position: x.position
         }));
      },

      async forgotPassword(ctx) {
         let x = ctx.params
         let password
         if (!x.phone || x.email == null || x.email.length == 0) {
            let resultMessage = await ctx.call('resultMessage.get', {id: '0002'})
            throw Error(resultMessage.message)
            //	throw Error('Email ve Telefon numarası zorunludur.');
         }

         let user = await this.adapter.findOne({email: x.email, phone: x.phone})

         if (user) {
            password = generate('123456789', 6)
            if (process.env.ROUTERCOMPANY === "smarteventlogo") {
               let logosmsresult = await fetch('http://gw.barabut.com/v1/xml/syncreply/Submit', {
                  method: 'POST',
                  headers: {
                     'Content-Type': 'text/xml; charset=utf-8',
                  },
                  body: `<Submit xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="SmsApi">
								<Credential>
								<Password>141153</Password>
								<Username>logo</Username>
								</Credential>
								<DataCoding>UCS2</DataCoding>
								<Header>
								<From>LOGOYAZILIM</From>
								<ValidityPeriod>0</ValidityPeriod>
								</Header>
								<Message> ${contentMessage.replace('%s', ctx.params.password)}</Message>
								<To xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
								<d2p1:string>${ctx.params.phone.replace('+', '')}</d2p1:string>
								</To>
							 </Submit>`,
               })
            } else {
               let smsresult = await fetch('http://edesms.com/Api/Submit', {
                  method: 'POST',
                  headers: {
                     'Content-Type': 'text/xml; charset=utf-8',
                  },
                  body: `<Submit xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="SmsApi">
						<Credential>
						<Password>7273202</Password>
						<Username>arneca</Username>
						</Credential>
						<DataCoding>Default</DataCoding>
						<Header>
						<From>Arneca</From>
						<ValidityPeriod>0</ValidityPeriod>
						</Header>
						<Message>Sevgili kullanici, etkinlik uygulamasini kullandigin icin tesekkur ederiz. Giris kodun: ${password}. Kodu ilgili alana yazarak uygulamayi kullanmaya hemen baslayabilirsin.</Message>
						<To xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
						<d2p1:string>${ctx.params.phone.replace('+', '')}</d2p1:string>
						</To>
						</Submit>`,
               })
            }
            let id = user._id
            this.adapter.updateById(id, {
               $set: {
                  password: crypto.createHmac('sha256', secret)
                     .update(password)
                     .digest('hex')
               }
            })
         } else {
            let resultMessage = await ctx.call('resultMessage.get', {id: '0028'})
            throw Error(resultMessage.message)
            //	throw Error('Girmiş olduğunuz bilgilere ait kullanıcı bulunamadı.');
         }
         return {...x, message: 'Şifreniz SMS olarak telefonunuza gönderilmiştir.'}
      },

      async leaveUsers(ctx) {

         let deletes = ctx.params.ids.filter(user => user.deleted).map(user => user._id)
         let actives = ctx.params.ids.filter(user => !user.deleted).map(user => user._id)

         if (deletes && deletes.length > 0) {
            deletes.forEach(async id => {
               this.adapter.updateById(id, {
                  $set: {
                     deleted: true
                  }
               })
               let userInfo = await ctx.call('registeredUsers.get', {id})
               await ctx.call('login.create', {
                  ...userInfo,
                  _id: nanoid(25),
                  userId: userInfo._id,
                  date: new Date(),
                  'type': 'Silindi'
               });
            })
         }


         if (actives && actives.length > 0)
            actives.forEach(id => this.adapter.updateById(id, {
               $set: {
                  deleted: false
               }
            }))

         return true;
      },

      async confirm(ctx) {
         let user = await this._get(ctx, {id: ctx.params.id})
         if (user.avatarUpload) {
            await this._update(ctx, {
               _id: ctx.params.id,
               avatar: user.avatarUpload,
               avatarUpload: null
            })
            await this.sendProfileNotification(ctx, ctx.params.id, user.avatarUpload, 'Profil resminiz onaylandı')
         }
         return true
      },

      async reject(ctx) {
         let user = await this._get(ctx, {id: ctx.params.id})
         if (user.avatarUpload) {
            await this._update(ctx, {
               _id: ctx.params.id,
               avatarUpload: null
            })
            await this.sendProfileNotification(ctx, ctx.params.id, user.avatarUpload, 'Profil fotoğrafınız uygun bulunmamıştır.')
         }
         return true
      },

      async search(ctx) {
         if (ctx.params.text) {
            let text = ctx.params.text;
            let reg = new RegExp('^' + text, 'i');
            let result = await this.adapter.find({query: {name: {$regex: reg}}});
            return result;
         } else
            return null;
      },

      async smsUpdate(ctx) {

         let {
            _id, password,
            ...others
         } = ctx.params;
         let id = _id
         this.adapter.updateById(id, {
            $set: {
               password: crypto.createHmac('sha256', secret)
                  .update(password)
                  .digest('hex')
            }
         })
         return true
      },

      async multipleCreate(ctx) {
         if (process.env.ROUTERCOMPANY !== 'kipas')
            return true
         let uname = 'IletisimUser', pass = '5tFs16PeWKymV'
         var basicAuth = 'Basic ' + Buffer.from(uname + ':' + pass).toString('base64')

         let {data} = await axios.get('https://vekil.kipas.com.tr/api/iletisim/GetUserList', {
            headers: {
               'Authorization': `${basicAuth}`
            }
         });


         let concatArray = data.map(eachValue => {
            if (eachValue.Phone.length > 9)
               return Object.values(eachValue.Phone).join('')
         })

         let filterValues = data.filter((value, index) => {
            return concatArray.indexOf(concatArray[index]) === index
         })
         let filterValueIds = filterValues.map(f => f.Sicil)

         let registeredUsers = await ctx.broker.call('registeredUsers.find', {
            query: {
               role: {$ne: "superAdmin"},
               deleted: {$ne: true},
               $where: "this._id.length <10"
            }, fields: ["_id"]
         }).map(u => u._id)
         let us = await ctx.broker.call('registeredUsers.find', {
            query: {updatedAt: {"$lte": new Date()}},
            fields: ["_id"]
         }).map(u => u._id)

         let difference = filterValues.filter(f => {
            return registeredUsers.indexOf(f.Sicil) < 0;
         })
         let differenceDelta = registeredUsers.filter(u => {
            return filterValueIds.indexOf(u) < 0;
         })

         let userToUpdate = us.map(u => filterValues.find(f => f.Sicil === u)).filter(f => f);

         if (ctx.params.return === "differance")
            return ({difference, differenceDelta, userToUpdate})
         if (ctx.params.deletedUsers && userToUpdate.length > 0) {
            differenceDelta.forEach(du => {
               this.adapter.updateById(du, {
                  $set: {
                     deleted: true
                  }
               })
            })
         }
         if (ctx.params.updatetoUsers && userToUpdate.length > 0) {
            await Promise.all(userToUpdate.map(async u => {
               if (u && u.Sicil) {
                  await this.adapter.updateById(u.Sicil, {
                     $set: {
                        email: u.Email,
                        name: u.Name,
                        lastname: u.LastName,
                        company: u.Company,
                        position: u.Position,
                        location: u.Location,
                        specialForMeProfileLink: u.ProfileLink,
                        department: u.Department,
                        jobPhone: u.Dahili,
                        birthday: u.BirthDate ? (u.BirthDate.split("-")[0].length < 2 ? "0" + u.BirthDate.split("-")[0].toString() : u.BirthDate.split("-")[0].toString()).toString() + "-" + (u.BirthDate.split("-")[1].length < 2 ? "0" + u.BirthDate.split("-")[1].toString() : u.BirthDate.split("-")[1].toString()).toString() : null,
                        updatedAt: new Date(),
                        deleted: false
                     }
                  })
               }
            }))
         } else if (ctx.params.addNewUsers && difference.length > 0) {
            await Promise.all(difference.forEach(async (u) => {
               if (u.Phone.length > 5) {
                  await ctx.call('registeredUsers.createAll', {...u}, {meta: {user: ctx.meta.user}})
               }
            }))
         } else throw Error('Listeye Eklenmiş Aktif Kullanıcı Bulunamadı.')

         return true
      },

      async createAll(ctx) {
         return await this.adapter.insert({...ctx.params})
      },

      async logincron(ctx) {
         let registeredUsers = await ctx.call('registeredUsers.find', {
            query: {
               deleted: {$ne: true}
            },
            fields: ["_id", "name", "email", "lastname", "role"]
         })
         let logins = await ctx.call('login.find', {
            fields: ["userId", "name", "email", "lastname", "date", "role", 'type'],
            sort: "userId,date"
         })

         let result = registeredUsers.map((user) => {
            let log = logins.find(l => l.userId === user._id ? l : false);
            if (log) {
               this.adapter.updateById(user._id, {
                  $set: {
                     isLogin: true
                  }
               })
            }
            return ({...user, log})
         })

         return true;

      },

      async groupsRemove(ctx) {
         let group_id = ctx.params.group_id;
         let registeredUsers = await ctx.broker.call("registeredUsers.find", {
            query: {
               groups: {$in: [group_id]}
            }
         })

         registeredUsers.forEach(async (element) => {
            const index = element.groups.indexOf(group_id);
            if (index > -1) {
               element.groups.splice(index, 1);
               let d = await this.adapter.updateById(element._id, {$set: {groups: element.groups}})
            }


         });

      },

      async count(ctx) {
         let registeredUsers = await this.adapter.find({})
         let activeUserCount = registeredUsers.filter(u => u.deleted !== true).length
         let loginUserCount = registeredUsers.filter(u => u.deleted !== true && u.isLogin).length
         let adminCount = registeredUsers.filter(u => u.deleted !== true && u.role === "admin").length
         return {activeUserCount, loginUserCount, adminCount}
      },

      async getUser(ctx) {
         let id = ctx.params.id
         let registeredUser = await ctx.call('registeredUsers.get', {id})
         // console.log("registeredUser", registeredUser)
         let subscriber
         if (registeredUser.isCorporate) {
            subscriber = await ctx.broker.call("subscriber.find", {
               query: {
                  VERGI_NO: registeredUser.vkNo,
               },
            });
         } else {
            subscriber = await ctx.broker.call("subscriber.find", {
               query: {
                  $and: [
                     {
                        $or: [
                           {VERGI_NO: {$eq: null}},
                           {VERGI_NO: {$eq: "null"}},
                        ]
                     },
                     {TC_NUMARASI: registeredUser.tcNo},
                  ],
               },
            });
         }
         let registeredUserId = {
            'avatar': registeredUser.avatar,
            'tcNo': registeredUser.tcNo || "",
            'vkNo': registeredUser.vkNo || "",
            'telNo': registeredUser.telNo || "",
            'AD': subscriber[0].AD || "",
            'SOYAD': subscriber[0].SOYAD || "",
         }
         // console.log("registeredUserId", registeredUserId)
         return registeredUserId
      }
   },

   methods: {
      async sendProfileNotification(ctx, id, avatar, title) {
         let user = await ctx.call('registeredUsers.get', {id});
         try {
            if (user && user.playerId)
               await axios.post('https://onesignal.com/api/v1/notifications', {
                  'app_id': process.env.ONESIGNAL_APPID,
                  'include_player_ids': [user.playerId],
                  'data': {
                     'title': title,
                     'item_id': null,
                     'type': 'notifications'
                  },
                  'contents': {'en': title}
               }, {
                  headers: {
                     'Authorization': 'Basic ' + process.env.ONESIGNAL_APIKEY
                  }
               });
            else this.logger.error('user player id not found');
         } catch (err) {
            this.logger.error(err);
         }
         await ctx.call('notifications.create', {
            title: title,
            active: true,
            content: title,
            date: new Date(),
            icon: avatar,
            media: avatar,
            userId: user._id,
            type: 'notifications',
            isRead: false
         });
      }
   },

   hooks: {
      before: {
			list: async ctx => {
            let query = ctx.params.query;
            if (!query)
               query = {}
            ctx.params.populate = ['groups'];

            if (query && query.deleted === 'true') {
               query.deleted = query && query.deleted === 'true';
            } else if (query && query.deleted === 'false') {
               query.deleted = {$ne: true};
            }

            // if(query && query.avatarUpload === 'true'){
            // 	query.avatarUpload = {$ne:null}
            // }
            // else if(query) delete query.avatarUpload;

            if (query && query.blood_donation === 'true') {
               query.blood_donation = query && query.blood_donation === 'true';
            } else if (query) delete query.blood_donation;

            if (query && query.allow_donation === 'true') {
               query.allow_donation = query && query.allow_donation === 'true';
            } else if (query) delete query.allow_donation;

            if (query && query.platelet_donation === 'true') {
               query.platelet_donation = query && query.platelet_donation === 'true';
            } else if (query) delete query.platelet_donation;

            let search = ctx.params.search
				if (search && search.length > 2) {
					let result = await ctx.broker.call('search.registeredUserSearch', {text: search})
					if (!ctx.params.query)
						ctx.params.query = {}
					ctx.params.query._id = {$in: result.map(r => r._id)}
				}
				delete ctx.params.search;
         },
         create: [async ctx => {
            if (ctx.params.jobStartDate === null) {
               delete ctx.params.jobStartDate
            }
            if (ctx.params.birthday === null) {
               delete ctx.params.birthday
            }
            if (ctx.params.jobStartDate) {
               ctx.params.jobStartDate = dayjs(ctx.params.jobStartDate).toDate();
            }
            if (ctx.params.birthday) {
               ctx.params.birthday = dayjs(ctx.params.birthday).format('DD-MM').toString();
            }

            if (ctx.params.email == null && ctx.params.phone == null) {
               let resultMessage = await ctx.call('resultMessage.get', {id: '0002'})
               throw Error(resultMessage.message)
               //	throw Error('Email veya telefon gereklidir')
            }

            if (ctx.params.password == null) {
               ctx.params.password = generate("1234567890", 6)
            }
            ctx.params.phone = ctx.params.phone.replace(/ /g, '')
            if (/^[0-9]{10}$/.exec(ctx.params.phone)) ctx.params.phone = '+90' + ctx.params.phone
            if (/^0*[0-9]{10}$/.exec(ctx.params.phone)) ctx.params.phone = '+9' + ctx.params.phone
            if (/^90*[0-9]{10}$/.exec(ctx.params.phone)) ctx.params.phone = '+' + ctx.params.phone


            let p = ctx.params;
            p._id = nanoid()
            p.createdAt = new Date();
            p.createdBy = ctx.meta.user._id
            p.updatedAt = new Date();
            ctx.params.updatedBy = ctx.meta.user._id;
            p.show_birthday = false;
            p.show_jobStart = false;
            p.password = crypto.createHmac('sha256', secret)
               .update(ctx.params.password)
               .digest('hex');
            p.show_blood = false;
            if (!(ctx.meta.user && (ctx.meta.user.role === 'admin' || ctx.meta.user.role === 'superAdmin')))
               p.role = "user"
            p.avatar = {
               '_id': 'yASVcVEwiz0fV8GzCqQAZGaC.png',
               'url': process.env.HOST + '/files/avatar.png',
               'thumb': process.env.HOST + '/files/avatar.png',
               'type': 'image',
               'mimeType': 'image/png'
            };
            let {
               _id,
               birthday,
               show_birthday,
               show_jobStart,
               avatar,
               blood,
               show_blood,
               groups,
               jobPhone,
               department,
               jobStartDate,
               allow_donation,
               allow_notification,
               blood_donation,
               location,
               lang,
               donation_cities,
               platelet_donation,
               playerId,
               createdAt,
               createdBy,
               updatedAt,
               updatedBy,
               facebook,
               twitter,
               instagram,
               linkedin,
               skype,
               password,
               role,
               name,
               lastname,
               phone,
               email,
               position,
               sendSms,
               sendMail,
               specialForMeProfileLink,
               company,
               registerNo,
               ...others

            } = ctx.params;

            if (email) [
               ctx.params.email = ctx.params.email.toLocaleLowerCase('tr')
            ]
            if (blood) {
               let find = bloodTypes.find(x => x === blood);
               if (find == null) {
                  let resultMessage = await ctx.call('resultMessage.get', {id: '0019'})
                  throw Error(resultMessage.message)
                  //	throw Error('Invalid blood type: ' + blood + '. Blood type must be one of ' + bloodTypes.join(', ')  );
               }
            }
            if (birthday) {
               let [day, month] = birthday.split('-');
               day = Number(day);
               month = Number(month);
               if (!(day && month))
                  delete ctx.params.birthday
            }


            if (others && Object.getOwnPropertyNames(others).length > 0)
               throw Error('invalid props: ' + Object.getOwnPropertyNames(others).join(', '));
            if (Object.getOwnPropertyNames(ctx.params).length === 0) {

               let resultMessage = await ctx.call('resultMessage.get', {id: '0004'})
               throw Error(resultMessage.message)
            }

            // try {

            let routerCompany = await axios.get('https://router.iciletisim.app/api/company/' + process.env.ROUTERCOMPANY);
            let loginType = routerCompany.data.result.loginType
            ctx.params.identityNumber = ctx.params.email;

            if (loginType == "phone") {
               ctx.params.identityNumber = ctx.params.phone;
               let checkUser = await ctx.call('registeredUsers.find', {
                  query: {
                     phone: ctx.params.phone,
                     $or: [{deleted: {$ne: true}}, {deleted: {$exists: false}}]
                  }
               });
               if (checkUser.length > 0) {
                  let resultMessage = await ctx.call('resultMessage.get', {id: '0034'})
                  throw Error(resultMessage.message)
               }
            } else {
               let checkUser = await ctx.call('registeredUsers.find', {
                  query: {
                     email: ctx.params.email,
                     $or: [{deleted: {$ne: true}}, {deleted: {$exists: false}}]
                  }
               });
               if (checkUser.length > 0) {
                  let resultMessage = await ctx.call('resultMessage.get', {id: '0015'})
                  throw Error(resultMessage.message)
               }
            }


            // Routera Ekle
            let routerResult = await axios.post('https://router.iciletisim.app/api/registeredUsers',
               {
                  identityNumber: ctx.params.identityNumber,
                  company: process.env.ROUTERCOMPANY
               }
            );

            if (sendSms) {

               let settings = await ctx.call('v0.settings.flat')
               let cmsMessages = settings.smsAndEmailMessage.cmsMessages
               let appName = settings.smsAndEmailMessage.appName
               let contentMessage = ""

               cmsMessages.forEach(element => {
                  if (lang == element.lang) {
                     contentMessage = element.content
                  }
               });
               if (process.env.ROUTERCOMPANY === "smarteventlogo") {
                  let logosmsresult = await fetch('http://gw.barabut.com/v1/xml/syncreply/Submit', {
                     method: 'POST',
                     headers: {
                        'Content-Type': 'text/xml; charset=utf-8',
                     },
                     body: `<Submit xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="SmsApi">
									<Credential>
									<Password>141153</Password>
									<Username>logo</Username>
									</Credential>
									<DataCoding>UCS2</DataCoding>
									<Header>
									<From>LOGOYAZILIM</From>
									<ValidityPeriod>0</ValidityPeriod>
									</Header>
									<Message> ${contentMessage}</Message>
									<To xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
									<d2p1:string>${ctx.params.phone.replace('+', '')}</d2p1:string>
									</To>
								 </Submit>`,
                  })
               } else {
                  let smsresult = fetch('http://edesms.com/Api/Submit', {
                     method: 'POST',
                     headers: {
                        'Content-Type': 'text/xml; charset=utf-8',
                     },
                     body: `<Submit xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="SmsApi">
											<Credential>
											<Password>7273202</Password>
											<Username>arneca</Username>
											</Credential>
											<DataCoding>Default</DataCoding>
											<Header>
											<From>${appName}</From>
											<ValidityPeriod>0</ValidityPeriod>
											</Header>
											<Message>${contentMessage}</Message>
											<To xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
											<d2p1:string>${ctx.params.phone.replace('+', '')}</d2p1:string>
											</To>
										</Submit>`,
                  })
               }
            }


            // } catch (err) {

            // }

         }],

         createAll: [async ctx => {
            let x = ctx.params
            //Parameter converter schema
            x._id = x.Sicil
            x.email = x.Email
            x.name = x.Name
            x.lastname = x.LastName
            x.phone = x.Phone
            x.company = x.Company
            x.position = x.Position
            x.location = x.Location
            x.birthday = x.BirthDate ? (x.BirthDate.split("-")[0].length < 2 ? "0" + x.BirthDate.split("-")[0].toString() : x.BirthDate.split("-")[0].toString()).toString() + "-" + (x.BirthDate.split("-")[1].length < 2 ? "0" + x.BirthDate.split("-")[1].toString() : x.BirthDate.split("-")[1].toString()).toString() : null
            x.blood_donation = x.Blood
            x.department = x.Department
            x.jobPhone = x.Dahili
            x.specialForMeProfileLink = x.ProfileLink

            if (x.email == null && x.phone == null) {
               let resultMessage = await ctx.call('resultMessage.get', {id: '0002'})
               throw Error(resultMessage.message)
               //  throw Error('Email veya telefon gereklidir')
            }
            if (x.password == null) {
               x.password = generate("1234567890", 6)
            }
            x.phone = x.phone.replace(/ /g, '')
            if (/^[0-9]{10}$/.exec(x.phone)) x.phone = '+90' + x.phone
            if (/^0*[0-9]{10}$/.exec(x.phone)) x.phone = '+9' + x.phone
            if (/^90*[0-9]{10}$/.exec(x.phone)) x.phone = '+' + x.phone

            let urlCompany = 'https://router.iciletisim.app/api/company/' + process.env.ROUTERCOMPANY
            let routerCompany = await axios.get(urlCompany);
            let loginType = routerCompany.data.result.loginType

            if (loginType == "phone") {
               let exist = await ctx.call('registeredUsers.find', {query: {phone: x.phone, deleted: {$ne: true}}})

               if (exist.length > 0) {
                  let resultMessage = await ctx.call('resultMessage.get', {id: '0034'})
                  throw Error(resultMessage.message)
               }
            } else {
               let exist = await ctx.call('registeredUsers.find', {query: {email: x.email.toLocaleLowerCase('tr')}})
               if (exist.length > 0) {
                  let resultMessage = await ctx.call('resultMessage.get', {id: '0015'})
                  throw Error(resultMessage.message)
                  //  throw new Error('Bu e-posta ile bir hesap mevcut')
               }
            }

            delete x.companyId
            delete x.companyName
            delete x.loginType
            delete x.Sicil
            delete x.Email
            delete x.Name
            delete x.LastName
            delete x.Phone
            delete x.Company
            delete x.Position
            delete x.Location
            delete x.BirthDate
            delete x.Blood
            delete x.ProfileLink
            delete x.ids
            delete x.Department
            delete x.Dahili
            delete x.Id
            delete x.Status

            x.createdAt = new Date();
            x.createdBy = ctx.meta.user._id || 'System'
            x.updatedAt = new Date();
            x.updatedBy = ctx.meta.user._id || 'System';
            x.show_birthday = false;
            x.show_jobStart = false;
            x.password = crypto.createHmac('sha256', secret)
               .update(x.password)
               .digest('hex');
            x.show_blood = false;
            if (!(ctx.meta.user && ctx.meta.user.role === 'admin'))
               x.role = "user"
            x.avatar = {
               '_id': 'yASVcVEwiz0fV8GzCqQAZGaC.png',
               'url': process.env.HOST + '/files/avatar.png',
               'thumb': process.env.HOST + '/files/avatar.png',
               'type': 'image',
               'mimeType': 'image/png'
            };
            let {
               _id,
               birthday,
               show_birthday,
               show_jobStart,
               avatar,
               blood,
               show_blood,
               groups,
               department,
               jobPhone,
               allow_donation,
               allow_notification,
               blood_donation,
               location,
               lang,
               donation_cities,
               platelet_donation,
               playerId,
               createdAt,
               createdBy,
               updatedAt,
               updatedBy,
               password,
               role,
               name,
               lastname,
               phone,
               email,
               position,
               identityNumber,
               sendSms,
               sendMail,
               specialForMeProfileLink,
               company,
               ...others
            } = ctx.params;

            if (email) [
               x.email = x.email.toLocaleLowerCase('tr')
            ]
            if (blood) {
               let find = bloodTypes.find(x => x === blood);
               if (find == null) {

                  let resultMessage = await ctx.call('resultMessage.get', {id: '0019'})
                  throw Error(resultMessage.message)
                  //  throw Error('Invalid blood type: ' + blood + '. Blood type must be one of ' + bloodTypes.join(', ')  );
               }

            }
            if (birthday) {
               let [day, month] = birthday.split('-');
               day = Number(day);
               month = Number(month);
               //delete params.birthday
               if (!(day && month))
                  delete x.birthday

               //  throw Error('invalid birtday: ' + birthday);
            }
            if (others && Object.getOwnPropertyNames(others).length > 0)
               throw Error('invalid props: ' + Object.getOwnPropertyNames(others).join(', '));
            if (Object.getOwnPropertyNames(ctx.params).length === 0) {

               let resultMessage = await ctx.call('resultMessage.get', {id: '0004'})
               throw Error(resultMessage.message)
               //  throw Error('invalid register. fields cent empty');
            }
         }],

         update: [async function (ctx) {
            let user = ctx.meta.user
            let {
               _id,
               birthday,
               show_birthday,
               show_jobStart,
               avatar,
               blood,
               show_blood,
               groups,
               jobPhone,
               department,
               jobStartDate,
               allow_donation,
               allow_notification,
               blood_donation,
               location,
               lang,
               donation_cities,
               platelet_donation,
               playerId,
               isLogin,
               isLoginTime,
               isKvkk,
               position,
               facebook,
               twitter,
               instagram,
               linkedin,
               skype,
               email,
               name,
               lastname,
               createdAt,
               createdBy,
               updatedAt,
               updatedBy,
               role,
               password,
               code,
               phone,
               registerNo,
               identityNumber,
               sendSms,
               sendMail,
               specialForMeProfileLink,
               company,
               deleted,
               ...others
            } = ctx.params;

            delete ctx.params.createdAt;
            delete ctx.params.createdBy;
            if (deleted && user.role === "user")
               return false
            ctx.params.updatedAt = new Date();
            ctx.params.updatedBy = user._id;

            if (ctx.params.jobStartDate === null) {
               delete ctx.params.jobStartDate
            }
            if (ctx.params.birthday === null) {
               delete ctx.params.birthday
            }


            if (ctx.params.jobStartDate) {
               ctx.params.jobStartDate = dayjs(ctx.params.jobStartDate).toDate();
            }

            if (ctx.params.birthday) {
               ctx.params.birthday = dayjs(ctx.params.birthday).format('DD-MM').toString();
            }

            if (ctx.params.role) {
               if (ctx.meta.user) {
                  if (!(ctx.meta.user.role === 'admin' || ctx.meta.user.role === 'superAdmin'))
                     delete ctx.params.role
               } else {
                  delete ctx.params.role
               }
            }

            if (password) {
               ctx.params.password = crypto.createHmac('sha256', secret)
                  .update(ctx.params.password)
                  .digest('hex');
            }
            if (email) {
               ctx.params.email = ctx.params.email.toLocaleLowerCase('tr')
            }

            if (groups) {
               let user = await this.broker.call('registeredUsers.get', {
                  id: _id,
               });
               if (JSON.stringify(user.groups) !== JSON.stringify(groups)) {
                  //token silme
                  await ctx.broker.call('tokens.userIdRemove', {userId: user._id});
               }
            }

            if (blood) {
               let find = bloodTypes.find(x => x === blood);
               if (find == null) {

                  let resultMessage = await ctx.call('resultMessage.get', {id: '0019'})
                  throw Error(resultMessage.message)
                  //	throw Error('Invalid blood type: ' + blood + '. Blood type must be one of ' + bloodTypes.join(', ')  );
               }
            }
            // if (!avatar || !avatar.thumb || !avatar.url) {
            // 	ctx.params.avatar = {
            // 		'_id': '27SVcVEwiz0fV8GzCqQAZG46.png',
            // 		'url': process.env.HOST + '/files/avatar.png',
            // 		'thumb': process.env.HOST + '/files/avatar.png',
            // 		'type': 'image',
            // 		'mimeType': 'image/png'
            // 	};
            // }

            if (role !== 'admin' && others
               && Object.getOwnPropertyNames(others).length > 0)
               throw Error('invalid props: ' + Object.getOwnPropertyNames(others).join(', '));
            if (Object.getOwnPropertyNames(ctx.params).length === 0) {

               let resultMessage = await ctx.call('resultMessage.get', {id: '0005'})
               throw Error(resultMessage.message)
               //	throw Error('invalid update. no fields to update');
            }
            if (birthday) {
               let [day, month] = birthday.split('-');
               day = Number(day);
               month = Number(month);
               //delete params.birthday
               if (!(day && month))
                  delete ctx.params.birthday
               //	throw Error('invalid birtday: ' + birthday);
            }

            // try {

            let oldData = await ctx.call('registeredUsers.get', {id: ctx.params._id})

            let routerCompany = await axios.get('https://router.iciletisim.app/api/company/' + process.env.ROUTERCOMPANY);
            let loginType = routerCompany.data.result.loginType


            identityNumber = oldData.email;
            let identityNumber2 = ctx.params.email;

            if (loginType == "phone") {
               identityNumber = oldData.phone;
               identityNumber2 = ctx.params.phone;

               if (identityNumber !== identityNumber2) {
                  let checkUser = await ctx.call('registeredUsers.find', {
                     query: {
                        phone: ctx.params.phone,
                        _id: {$ne: _id},
                        $or: [{deleted: {$ne: true}}, {deleted: {$exists: false}}]
                     }
                  });
                  if (checkUser.length > 0) {
                     let resultMessage = await ctx.call('resultMessage.get', {id: '0034'})
                     throw Error(resultMessage.message)
                  }
               }

            } else {
               if (identityNumber !== identityNumber2) {
                  let checkUser = await ctx.call('registeredUsers.find', {
                     query: {
                        email: ctx.params.email,
                        _id: {$ne: _id},
                        $or: [{deleted: {$ne: true}}, {deleted: {$exists: false}}]
                     }
                  });
                  if (checkUser.length > 0) {
                     let resultMessage = await ctx.call('resultMessage.get', {id: '0015'})
                     throw Error(resultMessage.message)

                  }
               }
            }
            // Routerı düzenle
            await axios.post(
               'https://router.iciletisim.app/api/registeredUsers/usersUpdate',
               {
                  identityNumber: identityNumber,
                  identityNumber2: identityNumber2,
                  company: process.env.ROUTERCOMPANY
               }
            );

            if (sendSms) {
               let settings = await ctx.call('v0.settings.flat')
               let cmsMessages = settings.smsAndEmailMessage.cmsMessages
               let appName = settings.smsAndEmailMessage.appName
               let contentMessage = ""

               cmsMessages.forEach(element => {
                  if (lang == element.lang) {
                     contentMessage = element.content
                  }
               });
               if (process.env.ROUTERCOMPANY === "smarteventlogo") {
                  let logosmsresult = await fetch('http://gw.barabut.com/v1/xml/syncreply/Submit', {
                     method: 'POST',
                     headers: {
                        'Content-Type': 'text/xml; charset=utf-8',
                     },
                     body: `<Submit xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="SmsApi">
									<Credential>
									<Password>141153</Password>
									<Username>logo</Username>
									</Credential>
									<DataCoding>UCS2</DataCoding>
									<Header>
									<From>LOGOYAZILIM</From>
									<ValidityPeriod>0</ValidityPeriod>
									</Header>
									<Message> ${contentMessage}</Message>
									<To xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
									<d2p1:string>${ctx.params.phone.replace('+', '')}</d2p1:string>
									</To>
								 </Submit>`,
                  })
               } else {
                  let smsresult = fetch('http://edesms.com/Api/Submit', {
                     method: 'POST',
                     headers: {
                        'Content-Type': 'text/xml; charset=utf-8',
                     },
                     body: `<Submit xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="SmsApi">
											<Credential>
											<Password>7273202</Password>
											<Username>arneca</Username>
											</Credential>
											<DataCoding>Default</DataCoding>
											<Header>
											<From>${appName}</From>
											<ValidityPeriod>0</ValidityPeriod>
											</Header>
											<Message>${contentMessage}</Message>
											<To xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
											<d2p1:string>${ctx.params.phone.replace('+', '')}</d2p1:string>
											</To>
										 </Submit>`,
                  })
               }
            }


            // } catch (err) {

            // }


            return ctx
         }],

         get: [(ctx) => {
            let playerId = ctx.params.playerId;
            if (ctx.meta.user && playerId) {
               let user = ctx.meta.user;
               ctx.broker.call('registeredUsers.update', {_id: user._id, playerId}, {meta: ctx.meta});
            }
         }]
      },

      after: {
         list: [async (ctx, result) => {
         
            for (const x of result.rows) {
               let [subscriber] = await ctx.broker.call('subscriber.getByField', {
                  field: x.isCorporate === true ? "VERGI_NO" : "TC_NUMARASI",
                  value: x.isCorporate === true ? x.vkNo : (x.tcNo || x.tcno)
               }, {meta: ctx.meta});
               if (subscriber) {
                  x.email = subscriber.MAIL || x.email||''
                  x.name = subscriber.AD || ''
                  x.phone = subscriber.TEL_NO || ''
                  x.lastname = subscriber.SOYAD || ''
               }
               //delete x.role
               delete x.password;
               delete x.qr_code;
               x.birthday;
               x.blood;
               x.show_blood;
               x.show_birthday;
               x.show_jobStart;
            }
            return result;
         }],

         find: [
            (ctx, result) => {
               result.forEach(x => {
                  delete x.password;
                  //delete x.role
                  delete x.qr_code;
                  delete x.birthday;
                  delete x.blood;
                  delete x.show_blood;
                  delete x.show_birthday;
                  delete x.show_jobStart;
               });
               return result;
            }
         ],


         get: [async (ctx, result) => {
            for (const key in result) {
               let [subscriber] = await ctx.broker.call('subscriber.getByField', {
                  field: result[key].isCorporate === true ? "VERGI_NO" : "TC_NUMARASI",
                  value: result[key].isCorporate === true ? result[key].vkNo : (result[key].tcNo || result[key].tcno)
               }, {meta: ctx.meta});
               if (subscriber) {   
                  result[key].email = subscriber.MAIL || ''
                  result[key].name = subscriber.AD || ''
                  result[key].phone = subscriber.TEL_NO || ''
                  result[key].lastname = subscriber.SOYAD || ''
                  result[key].vkNo = subscriber.VERGI_NO || ''
               }
            }
            return result;
         }
         ],
         createAll: [async (ctx, result) => {

            let urlCompany = 'https://router.iciletisim.app/api/company/' + process.env.ROUTERCOMPANY
            let {data} = await axios.get(urlCompany);
            let loginType = data.result.loginType

            if (loginType == "phone") {
               let url = 'https://router.iciletisim.app/api/registeredUsers'
               let routerUser = await axios.post(
                  url,
                  {
                     identityNumber: result.phone,
                     company: process.env.ROUTERCOMPANY
                  }
               );
               if (routerUser.data.result == null)
                  throw Error(routerUser.data.result_message.message)


               if (ctx.params.sendSms) {
                  let settings = await ctx.call('v0.settings.flat')
                  let cmsMessages = settings.smsAndEmailMessage.cmsMessages
                  let appName = settings.smsAndEmailMessage.appName
                  let contentMessage = ""

                  cmsMessages.forEach(element => {
                     if (lang == element.lang) {
                        contentMessage = element.content
                     }
                  });
                  if (process.env.ROUTERCOMPANY === "smarteventlogo") {
                     let logosmsresult = await fetch('http://gw.barabut.com/v1/xml/syncreply/Submit', {
                        method: 'POST',
                        headers: {
                           'Content-Type': 'text/xml; charset=utf-8',
                        },
                        body: `<Submit xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="SmsApi">
										<Credential>
										<Password>141153</Password>
										<Username>logo</Username>
										</Credential>
										<DataCoding>UCS2</DataCoding>
										<Header>
										<From>LOGOYAZILIM</From>
										<ValidityPeriod>0</ValidityPeriod>
										</Header>
										<Message> ${contentMessage}</Message>
										<To xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
										<d2p1:string>${ctx.params.phone.replace('+', '')}</d2p1:string>
										</To>
									 </Submit>`,
                     })
                  } else {
                     let smsresult = fetch('http://edesms.com/Api/Submit', {
                        method: 'POST',
                        headers: {
                           'Content-Type': 'text/xml; charset=utf-8',
                        },
                        body: `<Submit xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="SmsApi">
													<Credential>
													<Password>7273202</Password>
													<Username>arneca</Username>
													</Credential>
													<DataCoding>Default</DataCoding>
													<Header>
													<From>${appName}</From>
													<ValidityPeriod>0</ValidityPeriod>
													</Header>
													<Message>${contentMessage}</Message>
													<To xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
													<d2p1:string>${ctx.params.phone.replace('+', '')}</d2p1:string>
													</To>
												</Submit>`,
                     })
                  }
               }

               result.identityNumber = result.phone;
            } else {
               let url = 'https://router.iciletisim.app/api/registeredUsers'
               let routerUser = await axios.post(
                  url,
                  {
                     identityNumber: result.email.toLocaleLowerCase('tr'),
                     company: process.env.ROUTERCOMPANY
                  }
               );
               if (routerUser.data.result == null)
                  throw Error(routerUser.data.result_message.message)
               result.identityNumber = result.email;

            }
            return result
         }]
      }
   }

};
